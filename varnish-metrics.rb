#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class ZVar < Sensu::Plugin::Metric::CLI::Graphite

#  option :scheme,
#    :description => "Metric naming scheme, text to prepend to .$parent.$child",
#    :long => "--scheme SCHEME",
#    :default => "#{Socket.gethostname}.zabbix"

        def check(temp)
                if temp.chomp! != "0"
                exit
        end
        end
  
	def check_release
        	`lsb_release -c`
	end

          def run
                hostname = "#{Socket.gethostname}.zabbix"
		debian_ver = check_release
                check(`pidof varnishd >/dev/null ; echo $?`)
                if debian_ver["stretch"]
			hit = `varnishstat -1`.split("\n").grep(/MAIN.cache_hit .*/)[0].split()[1] 
			miss = `varnishstat -1`.split("\n").grep(/MAIN.cache_miss .*/)[0].split()[1]
			nuked = `varnishstat -1`.split("\n").grep(/MAIN.n_lru_nuked .*/)[0].split()[1]
                elsif debian_ver["jessie"]
                        hit = `varnishstat -1 -f MAIN.cache_hit | ruby -lane 'print $F[1]'`
                        miss = `varnishstat -1 -f MAIN.cache_miss | ruby -lane 'print $F[1]'`
                        nuked = (`varnishstat -1 -f MAIN.n_lru_nuked | ruby -lane 'print $F[1]'`).chomp!
                elsif debian_ver["wheezy"]
                        hit = `varnishstat -1 -f cache_hit | ruby -lane 'print $F[1]'`
                        miss = `varnishstat -1 -f cache_miss | ruby -lane 'print $F[1]'`
                        nuked = (`varnishstat -1 -f n_lru_nuked | ruby -lane 'print $F[1]'`).chomp!
                else
                        exit
                end

                fhit = hit.to_i
                fmiss = miss.to_i

                if fhit == 0
                        hitrate = 0
                else
                        hitrate = ((fhit *100 )/(fhit + fmiss)).round(4)
                end

               timestamp = Time.now.to_i

        metrics = {
                :varnish => {
                        :hitrate => hitrate,
                        :nuked => nuked
                }
        }
        metrics.each do |parent, children|
        children.each do |child, value,|
        output "#{parent},type=metric #{child}=#{value} #{timestamp}000000000"
      end
    end
    ok
  end
end

