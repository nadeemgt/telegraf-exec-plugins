#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
require 'net/http'
require 'uri'

$playbook_fact_file = "/etc/ansible/facts.d/playbook_version.fact"
$url = "http://cloudways-static-content.s3-website-us-east-1.amazonaws.com/playbook_version/current.txt"

class PlaybookVersion < Sensu::Plugin::Metric::CLI::JSON

  def initialize
    super
    @crit_fs = [] 
    @warning_fs = []
    @line_count = 0
    @warn = {}
    @crit = {}
    @payload = { "type"=> "eng_analytics", "code"=>0, "output"=>"" }
  end

  def run
	
    latest_version = fetch_latest_version

    # Read facts file and get current version
    if File.readlines($playbook_fact_file).grep(/playbook/).any?
      current_version = File.readlines($playbook_fact_file).grep(/playbook/)
      current_version = current_version[0].split('=')[1].to_i
    else
      @payload["code"] = 3
      @payload["output"] = "Playbook version not found in facts file"
      ok @payload.to_json
      #unknown "Playbook version not found in facts file."
    end

    if current_version == latest_version
      @payload["code"] = 0
      @payload["output"] = "Up-to-date with version #{latest_version}."
      ok @payload.to_json
    elsif current_version > latest_version or current_version == 0
      @payload["code"] = 3
      @payload["output"] = "Current version: #{current_version}, Latest version: #{latest_version}"
      ok @payload.to_json
    else
      @payload["code"] = 2
      @payload["output"] = "Current version: #{current_version}, Latest version: #{latest_version}"
      ok @payload.to_json
    end
  end

  def fetch_latest_version
    uri = URI.parse($url)
    response = Net::HTTP.get_response(uri)

    if response.code.to_i != 200
      @payload["code"] = 3
      @payload["output"] = "Error #{response.code} while fetching latest version"
      ok @payload.to_json
    end
    return response.body.to_i
  end

end
