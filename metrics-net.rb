#!/usr/bin/env ruby
#
# Linux network interface metrics
# ====
#
# Simple plugin that fetchs metrics from all interfaces
# on the box using the /sys/class interface.
#
# Use the data with graphite's `nonNegativeDerivative()` function
# to construct per-second graphs for your hosts.
#
# Loopback iface (`lo`) is ignored.
#
# Compat
# ------
#
# This plugin uses the `/sys/class/net/<iface>/statistics/{rx,tx}_*`
# files to fetch stats. On older linux boxes without /sys, this same
# info can be fetched from /proc/net/dev but additional parsing
# will be required.
#
# Example:
# --------
#
# $ ./metrics-packets.rb --scheme servers.web01
#   servers.web01.eth0.tx_packets 982965    1351112745
#   servers.web01.eth0.rx_packets 1180186   1351112745
#   servers.web01.eth1.tx_packets 273936669 1351112745
#   servers.web01.eth1.rx_packets 563787422 1351112745
#
# Copyright 2012 Joe Miller <https://github.com/joemiller>
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class LinuxPacketMetrics < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to metric",
    :short => "-s SCHEME",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}.net"

  option :packets,
    :description => 'print only rx_tx_packets',
    :long => '--packets',
    :required => false

  option :errors,
    :description => 'print only rx_tx_errors',
    :long => '--errors',
    :required => false

  def get_provider
    facts_file = '/etc/ansible/facts.d/cloud_provider.fact'
    cloud = File.readlines(facts_file).grep(/cloud/)[0].split('=')[1].chomp()
    return cloud
  rescue
    return 'do'
  end

  def get_kyup_stats
    # cat /proc/net/dev
    # Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # ip6tnl0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
    # tunl0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
    # lo:  513739    2970    0    0    0     0          0         0   513739    2970    0    0    0     0       0          0
    # eth0: 139510508  340429    0    0    0     0          0         0  2476489   20366    0    0    0     0       0          0
    # eth1:    6832      84    0    0    0     0          0         0    12358     141    0    0    0     0       0          0

    stats_file = '/proc/net/dev'
    collection_regex = /^(\w*):\s*(\d*)\s*(\d*)\s*(\d*)(?:\s*\d*){5}\s*(\d*)\s*(\d*)\s*(\d*)/
    timestamp = Time.now.to_i

    File.open(stats_file, "r") do |s_file|
      s_file.each_line do |interface|
        interface.strip!

        if interface.start_with?('eth')
          matchdata = interface.match(collection_regex)
          iface = matchdata.captures[0]
          rx_bytes= matchdata.captures[1]
          rx_pkts= matchdata.captures[2]
          rx_errors= matchdata.captures[3]
          tx_bytes= matchdata.captures[4]
          tx_pkts= matchdata.captures[5]
          tx_errors= matchdata.captures[6]
          if config[:packets] 
            output "#{config[:scheme]}.#{iface}.tx_packets", tx_pkts, timestamp
            output "#{config[:scheme]}.#{iface}.rx_packets", rx_pkts, timestamp
          end
          if config[:errors]
            output "#{config[:scheme]}.#{iface}.tx_errors", tx_errors, timestamp
            output "#{config[:scheme]}.#{iface}.rx_errors", rx_errors, timestamp
          end
          output "#{config[:scheme]}.#{iface}.tx_bytes", tx_bytes, timestamp
          output "#{config[:scheme]}.#{iface}.rx_bytes", rx_bytes, timestamp
        end

      end
    end
  end

  def run
    timestamp = Time.now.to_i

    cloud = get_provider()

    if cloud == 'kyup'
      get_kyup_stats
    else
      Dir.glob('/sys/class/net/*').each do |iface_path|
        iface = File.basename(iface_path)
        next if iface == 'lo'

        tx_pkts = File.open(iface_path + '/statistics/tx_packets').read.strip
        rx_pkts = File.open(iface_path + '/statistics/rx_packets').read.strip
        tx_bytes = File.open(iface_path + '/statistics/tx_bytes').read.strip
        rx_bytes = File.open(iface_path + '/statistics/rx_bytes').read.strip
        tx_errors = File.open(iface_path + '/statistics/tx_errors').read.strip
        rx_errors = File.open(iface_path + '/statistics/rx_errors').read.strip
        if config[:packets] 
          output "#{config[:scheme]}.#{iface}.tx_packets", tx_pkts, timestamp
          output "#{config[:scheme]}.#{iface}.rx_packets", rx_pkts, timestamp
        end
        if config[:errors]
          output "#{config[:scheme]}.#{iface}.tx_errors", tx_errors, timestamp
          output "#{config[:scheme]}.#{iface}.rx_errors", rx_errors, timestamp
        end
        output "#{config[:scheme]}.#{iface}.tx_bytes", tx_bytes, timestamp
        output "#{config[:scheme]}.#{iface}.rx_bytes", rx_bytes, timestamp
      end
    end
    ok
  end
end

