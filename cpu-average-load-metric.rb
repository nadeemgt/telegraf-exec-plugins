#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

if RUBY_VERSION < '1.9.0'
  require 'bigdecimal'

  class Float
    def round(val = 0)
       BigDecimal.new(self.to_s).round(val).to_f
    end
  end
end

class LoadStat < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}.cpu"

  option :per_core,
    :description => 'Divide load average results by cpu/core count',
    :short => "-p",
    :long => "--per-core",
    :boolean => true,
    :default => true

  def number_of_cores
    @cores ||= File.readlines('/proc/cpuinfo').select { |l| l =~ /^processor\s+:/ }.count
  end

  def run
    result = `uptime`.gsub(',', '').split(' ')
    result = result[-3..-1]


            timestamp = Time.now.to_i
            cload = (result[0].to_f / number_of_cores).round(2) * 100

        metrics = {
                load: {
                one: result[0],
                five: result[1],
                fifteen: result[2]
                }
        }

        metrics.each do |parent, children|
        children.each do |child, value,|
        output [config[:scheme], parent, child].join("."), value, timestamp
      end
    end
    ok
  end

end
