#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'

C_SUCCESS = '0'
C_WARN = '1'
C_FAIL = '2'
C_UNKNOWN = '3'

class CheckDisk < Sensu::Plugin::Metric::CLI::Graphite

  option :fstype,
    :short => '-t TYPE[,TYPE]',
    :description => 'Only check fs type(s)',
    :proc => proc {|a| a.split(',') },
    :default => 'ext4'

  option :warn,
    :short => '-w PERCENT',
    :description => 'Warn if PERCENT or more of disk full',
    :proc => proc {|a| a.to_i },
    :default => 90

  option :crit,
    :short => '-c PERCENT',
    :description => 'Critical if PERCENT or more of disk full',
    :proc => proc {|a| a.to_i },
    :default => 95

  option :debug,
    :short => '-d',
    :long => '--debug',
    :description => 'Output list of included filesystems'


  # AWS or DO
  cloud_check=`df -h | grep "xvda\\|sdb\\|BLOCKSTORAGE\\|nvme0n1p2" | wc -l`

  case cloud_check
  when /1/

    option :ignoremnt,
      :short => '-i MNT[,MNT]',
      :description => 'Ignore mount point(s)',
      :proc => proc {|a| a.split(',') },
      :default => '/'

  else

    option :ignoremnt,
      :short => '-i MNT[,MNT]',
      :description => 'Ignore mount point(s)',
      :proc => proc {|a| a.split(',') }
  end


  def initialize
    super
    @crit_fs = []
    @warn_fs = []
    @warn = {}
    @crit = {}
    @unknown = {}
    @output ={}
    @payload = {"type"=>"alert", "immediate"=>true}
    @line_count = 0
  end
  def read_df
    `df -PT`.split("\n").drop(1).each do |line|
      begin
        _fs, type, _blocks, _used, _avail, capacity, mnt = line.split
        next if config[:fstype] && !config[:fstype].include?(type)
        next if config[:ignoremnt] && config[:ignoremnt].include?(mnt)
        puts line if config[:debug]

      rescue
        @payload[C_UNKNOWN] = "malformed line from df: #{line}"
        critical @payload.to_json
      end
      @line_count += 1
      if capacity.to_i >= config[:crit]
        @crit_fs << "#{mnt} #{capacity}"
      elsif capacity.to_i >= config[:warn]
        @warn_fs <<  "#{mnt} #{capacity}"
      end
    end

  end

  def usage_summary

    (@crit_fs + @warn_fs).join(', ')
  end

  def run
    read_df
    if not @line_count > 0
        @payload["code"]=C_UNKNOWN.to_i
        @output[C_UNKNOWN] = 'No filesystems found'
    elsif not @crit_fs.empty?
	@payload["code"]=C_FAIL.to_i
        @output[C_FAIL] = usage_summary
    elsif not @warn_fs.empty?
       @payload["code"]=C_WARN.to_i
       @output[C_WARN] = usage_summary
    else
       @payload["code"]=C_SUCCESS.to_i
       @output[C_SUCCESS] = "All disk usage under #{config[:warn]}%"
    end
    @payload["output"] = @output.to_json
    ok @payload.to_json
  end

end

