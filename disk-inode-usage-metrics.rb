#! /usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class DiskCapacity < Sensu::Plugin::Metric::CLI::Graphite
  #option :scheme,
  #       description: 'Metric naming scheme, text to prepend to .$parent.$child',
  #       long: '--scheme SCHEME',
  #       default: "#{Socket.gethostname}.disk"

  def convert_integers(values)
    values.each_with_index do |value, index|
      begin
        converted = Integer(value)
        values[index] = converted
        # #YELLOW
      rescue ArgumentError # rubocop:disable HandleExceptions TEST
      end
    end
    values
  end

  def run
    # Get inode capacity metrics
    hostname = "#{Socket.gethostname}.disk"
    `df -Pi`.split("\n").drop(1).each do |line|
      begin
        fs, _inodes, used, avail, capacity, _mnt = line.split
        timestamp = Time.now.to_i
        if fs.match('/dev')
          fs = fs.gsub('/dev/', '')

          metrics = {
            inodes: {
              "#{fs}.utilized" => capacity.gsub('%', '')
            }
          }
          metrics.each do |parent, children|
            children.each do |child, value|
          if child.match('/by-')
              output "disk.#{parent},type=metric root.utilized=#{value} #{timestamp}000000000"
          else
              output "disk.#{parent},type=metric #{child}=#{value} #{timestamp}000000000"
          end
            end
          end
        end
      rescue
        unknown "malformed line from df: #{line}"
      end
    end
    ok
  end
end

