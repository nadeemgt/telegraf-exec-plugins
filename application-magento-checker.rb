#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
MAGENTO_JSON_FILE = '/etc/sensu/plugins/data/magentoPatchData.json'

class BackUP < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"


  def initialize
    super
    @crit_fs = []
    @line_count = 0
  end


def run

# Magento patch file
magento_patch_file = ""

# Magento mage file
magento_mage_file = ""

# FQDN
fqdn=`hostname`.chomp

vhost = `grep -lr "magento-*..*cloudwaysapps.com" /home/#{fqdn}/*/conf/server.nginx  |  awk -F"#{fqdn}/" '{print $2}' | awk -F"/conf" '{print $1}'`.split(' ', 0)


magento_date_dict = {}

        vhost.each do |enable|

# Magento mage file
magento_mage_file = "/home/#{fqdn}/#{enable}/public_html/app/Mage.php"

                if File.exists?(magento_mage_file)
                        begin
                                magento_current = `cd /home/#{fqdn}/#{enable}/public_html/; su #{enable} -s /bin/bash -c 'php -r "include'"'"'app/Mage.php'"'"'; echo Mage::getVersion();"| tail -n1'`.strip()
                                magento_date_dict[enable] = magento_current
                        end
                end
        end

        begin
                magentoPatchDict = ::JSON.parse(open(MAGENTO_JSON_FILE).read)

                magento_date_dict.each do |vhost, version|

# Magento patch file
magento_patch_file = "/home/#{fqdn}/#{vhost}/public_html/app/etc/applied.patches.list"

                        magentoPatchDict[version].each do |patch_name, status|
                                if status == 'Required'
                                        if File.exists?(magento_patch_file)

                                                patch_file = `grep -F '|' #{magento_patch_file} | grep -e "#{patch_name}" | tail -n1 | grep -v "REVERTED" |  wc -l`


                                                if patch_file < "1"
                                                        if patch_name == "SUPEE-2677" || patch_name == "SUPEE-2747"
                                                                @line_count += 1
                                                                @crit_fs << "#{vhost} with #{version} version requires patch APPSEC-212\n"
                                                        elsif  patch_name == "SUPEE-5341" || patch_name == "SUPEE-5344" || patch_name == "SUPEE-5345" || patch_name == "SUPEE-5346" || patch_name == "SUPEE-5388"  || patch_name == "SUPEE-5390"
                                                                @line_count += 1
                                                                @crit_fs << "#{vhost} with #{version} version requires patch SUPEE-5344\n"
                                                        else
                                                                @line_count += 1
                                                                @crit_fs << "#{vhost} with #{version} version requires patch #{patch_name}\n"
                                                        end
                                                end

                                        elsif patch_name == "SUPEE-2677" || patch_name == "SUPEE-2747"
                                                @line_count += 1
                                                @crit_fs << "#{vhost} with #{version} version requires patch APPSEC-212: NO PATCH FILE\n"
                                        elsif  patch_name == "SUPEE-5341" || patch_name == "SUPEE-5344" || patch_name == "SUPEE-5345" || patch_name == "SUPEE-5346" || patch_name == "SUPEE-5388"  || patch_name == "SUPEE-5390"
                                                @line_count += 1
                                                @crit_fs << "#{vhost} with #{version} version requires patch SUPEE-5344: NO PATCH FILE\n"
                                        else
                                                @line_count += 1
                                                @crit_fs << "#{vhost} with #{version} version requires patch #{patch_name}: NO PATCH FILE\n"
                                        end
                                end
                        end
                end

        rescue Exception => e
                print e
        end
                @crit_fs.uniq!
                critical @crit_fs.join("") unless @crit_fs.empty?
                ok "All Magento Applications are patched"

  end
end


