#! /usr/bin/env ruby
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'json'

C_SUCCESS = '0'
C_WARN = '1'
C_FAIL = '2'
C_UNKNOWN = '3'

class CheckDisk < Sensu::Plugin::Metric::CLI::Graphite
  option :fstype,
    short: '-t TYPE[,TYPE]',
    description: 'Only check fs type(s)',
    proc: proc { |a| a.split(',') },
    :default => 'ext4'

  option :ignoretype,
    short: '-x TYPE[,TYPE]',
    description: 'Ignore fs type(s)',
    proc: proc { |a| a.split(',') }


  # AWS or DO
  cloud_check=`df -h | grep "xvda\\|sdb\\|BLOCKSTORAGE\\|nvme0n1p2" | wc -l`

  case cloud_check
  when /1/

    option :ignoremnt,
      :short => '-i MNT[,MNT]',
      :description => 'Ignore mount point(s)',
      :proc => proc {|a| a.split(',') },
      :default => '/'

  else

    option :ignoremnt,
      :short => '-i MNT[,MNT]',
      :description => 'Ignore mount point(s)',
      :proc => proc {|a| a.split(',') }
  end


  option :ignoreline,
    short: '-l PATTERN[,PATTERN]',
    description: 'Ignore df line(s) matching pattern(s)',
    proc: proc { |a| a.split(',') }

  option :includeline,
    short: '-L PATTERN[,PATTERN]',
    description: 'Only include df line(s) matching pattern(s)',
    proc: proc { |a| a.split(',') }

  option :iwarn,
    short: '-W PERCENT',
    description: 'Warn if PERCENT or more of inodes used',
    proc: proc(&:to_i),
    default: 90

  option :icrit,
    short: '-K PERCENT',
    description: 'Critical if PERCENT or more of inodes used',
    proc: proc(&:to_i),
    default: 95

  option :debug,
    short: '-d',
    long: '--debug',
    description: 'Output list of included filesystems'

  def initialize
    super
    @crit_fs = []
    @warn_fs = []
    @warn = {}
    @crit = {}
    @unknown = {}
    @output = {}
    @payload = {"type"=>"alert", "immediate"=>true }
    @line_count = 0
  end

  def read_df
    `df -lPTi`.split("\n").drop(1).each do |line|
      begin
        _fs, type, _inodes, _used, _avail, capacity, mnt = line.split
        next if config[:includeline] && !config[:includeline].find { |x| line.match(x) }
        next if config[:fstype] && !config[:fstype].include?(type)
        next if config[:ignoretype] && config[:ignoretype].include?(type)
        next if config[:ignoremnt] && config[:ignoremnt].include?(mnt)
        next if config[:ignoreline] && config[:ignoreline].find { |x| line.match(x) }
        puts line if config[:debug]
      rescue
        @unknown[C_UNKNOWN] = "malformed line from df: #{line}"
        unknown @unknown.to_json
      end
      @line_count += 1
      if capacity.to_i > config[:icrit]
        @crit_fs << "#{mnt} inodes #{capacity}"
      elsif capacity.to_i >= config[:iwarn]
        @warn_fs << "#{mnt} inodes #{capacity}"
      end
    end
  end

  def usage_summary
    (@crit_fs + @warn_fs).join(', ')
  end
  def run
    if config[:includeline] && config[:ignoreline]
       @payload["code"] = 3
       @output["3"] = 'Do not use -l and -L options concurrently'
    else
       read_df
       if not @line_count > 0
          @payload["code"] = 3
	        @output["3"] = 'No filesystems found'
       elsif not @crit_fs.empty?
          @payload["code"] = 2
      	  @output[C_FAIL] = usage_summary
       elsif not @warn_fs.empty?
         @payload["code"] = 1
         @output[C_WARN] = usage_summary
       else
	 @payload["code"] = 0
         @output[C_SUCCESS] = "All disk inode usage under #{config[:iwarn]}%"
       end
    end
    @payload["output"] = @output.to_json
    ok @payload.to_json
  end
end
