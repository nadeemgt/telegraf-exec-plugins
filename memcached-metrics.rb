#! /usr/bin/env ruby
#

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
require 'timeout'

class ZMem < Sensu::Plugin::Metric::CLI::Graphite
  option :host,
         short: '-h HOST',
         long: '--host HOST',
         description: 'Memcached Host to connect to',
         default: 'localhost'

  option :port,
         short: '-p PORT',
         long: '--port PORT',
         description: 'Memcached Port to connect to',
         proc: proc(&:to_i),
         default: 11_211

  #option :scheme,
  #       description: 'Metric naming scheme, text to prepend to metric',
  #       short: '-s SCHEME',
  #      long: '--scheme SCHEME',
  #      default: "#{::Socket.gethostname}.zabbix"

  option :timeout,
         description: 'Timeout in seconds to complete the operation',
         short: '-t SECONDS',
         long: '--timeout SECONDS',
         proc: proc(&:to_i),
         default: 5

 def run
    hostname = "#{::Socket.gethostname}.zabbix"
    timestamp = Time.now.to_i
    mem_stats = memcache_stats
    mem_stats.each do |k, v|
      output "zabbix.memcached type=metric #{k}=#{v} #{timestamp}000000000" 
    end
    ok
 end

 def memcache_stats
    stat = {}
    stats = {}
      memcache_info.each_line do |line|
	_, key, value = line.split(' ', -1)
	stat['get_hits'] = value.to_i	if line.match(/^STAT get_hits/)
	stat['get_misses'] = value.to_i	if line.match(/^STAT get_misses/)
	stat['limit_maxbytes'] = value.to_i   if line.match(/^STAT limit_maxbytes/)
	stat['bytes'] = value.to_i   if line.match(/^STAT bytes/)
      end
	
      if stat['get_hits'] == 0 || stat['get_misses'] == 0
        stats['hitrate'] = 0
      else
        stats['hitrate'] = ((stat['get_hits'] * 100) / ( stat['get_hits'] + stat['get_misses'] )).round(2)
      end
 
      if stat['limit_maxbytes'] == 0 || stat['bytes'] == 0
        stats['fillratio'] = 0
      else
        stats['fillratio'] = ((stat['bytes'] *100 )/stat['limit_maxbytes']).round(4)
      end

    stats
 end 
 
 def memcache_info
    Timeout.timeout(config[:timeout]) do
      TCPSocket.open("#{config[:host]}", "#{config[:port]}") do |socket|
        socket.print "stats\r\n"
        socket.close_write
	socket.read
      end
    end
  rescue Timeout::Error
  rescue
    exit(0)
 end

end

