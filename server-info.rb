#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'
require 'net/http'
require 'uri'


GIT_HASH_S3 = "https://s3.amazonaws.com/cloudways-static-content/sensu-plugins/git_latest_hash.txt"
GIT_DIR = "/etc/sensu/plugins/"
GIT_HASH = `cd "#{GIT_DIR}"; git log --pretty=oneline -1 --format=%H`.chomp

C_SUCCESS = '0'
C_WARN = '1'
C_FAIL = '2'

class ServerInfo < Sensu::Plugin::Metric::CLI::Graphite

  option :git,
    :description => 'Git hash checker',
    :long => '--git',
    :required => false


  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def initialize
    super
    @output = {}
    @payload = {"type"=> "eng_analytics", "code"=>0, "output"=>""}
  end

  def run

    def fetch_latest_hash
      uri = URI.parse(GIT_HASH_S3)
      response = Net::HTTP.get_response(uri)

      if response.code.to_i != 200
        @payload["code"] = 1
        @payload["output"] = "Error #{response.code} while fetching latest hash"
        ok @payload.json
      end
      return response.body.chomp
    end

    if config[:git] 
      if fetch_latest_hash == GIT_HASH
	@payload["code"] = 0
        @output[C_SUCCESS] = "Hash is up-to-date with #{GIT_HASH}"
      else
	@payload["code"] = 2
        @output[C_FAIL] = "Hash is mismatched"
      end
    else
      @payload["code"] = 0
      @output[C_SUCCESS] = { "code" => `lsb_release -sc`.chomp, "release" => `lsb_release -sr`.chomp }
    end
    @payload["output"] = @output.to_json
    ok @payload.to_json
  end
end
