#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class ZXBand < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to .$parent.$child",
         :long => "--scheme SCHEME",
         :default => "zabbix"

  option :check_type,
         :description => "Generate output based on type either graphite or sqs",
         :short => "-c CHECK_TYPE",
         :long => "--check CHECK_TYPE",
         :default => "graphite"

  def run
    check_result = config[:scheme]

    # get default interface
    default_eth = `route | grep default`
    default_eth = default_eth.split().last()

    # Adding output of vnstat in variable
    vnstat_output = `vnstat -i #{default_eth} --oneline`
    vnstat_output = vnstat_output.split(";")

    # sqs output
    if config[:check_type] == "sqs"
      incoming = get_usage_in_bytes(vnstat_output[8])
      outgoing = get_usage_in_bytes(vnstat_output[9])
      total = incoming + outgoing

      check_result << " " << total.to_s
      check_result << " " << incoming.to_s
      check_result << " " << outgoing.to_s

    # graphite output
    else
      check_result << ".bandwidth,type=metric "
      check_result << "monthly=" << get_usage_in_mega_bytes(vnstat_output[10]).to_s
    end

    check_result << " " << Time.now.to_i.to_s << "000000000"
    output check_result
    ok #check_result
  end

  def get_usage_in_mega_bytes(val)
    val = val.split()
    usage = val[0].to_f
    unit = val[1]

    result = case unit
                when /KiB/
                  usage / 1024
                when /MiB/
                  usage
                when /GiB/
                  usage * 1024
                when /TiB/
                  usage * 1024 * 1024
                else
                  0
                end

    return result
  end

  def get_usage_in_bytes(val)
    usage = get_usage_in_mega_bytes(val)

    # convert into bytes
    if usage > 0
      usage = usage * 1024 * 1024
    end

    return usage
  end

end
