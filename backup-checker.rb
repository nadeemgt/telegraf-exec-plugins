#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'
require 'time'

C_SUCCESS = '0'
C_WARN = '1'
#C_FAIL = `cat backup.fact | grep error_code_ | awk -F"=" '{print $2}' 2>/dev/null`.split[0] 

FactFile="/etc/ansible/facts.d/backup.fact"

class BackUP < Sensu::Plugin::Metric::CLI::JSON

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"


  def initialize
    super
    @crit_fs = [] 
    @warning_fs = []
    @line_count = 0
    @warn = {}
    @crit = {}
    @output={}
    @payload = { "type"=>"alert", "code"=>0, "output"=>"", "immediate"=>true }
  end

  def run
    fqdn=`hostname`.chomp
    vhots = `ls /etc/nginx/sites-enabled/`.split(' ', 0)

    if vhots.length() < 1
        @payload["code"] = 0
        @output["0"] = "All Applications backup available"
        @payload["output"]=@output.to_json
        ok @payload.to_json
    end

    if not File.exists?(FactFile)
        @output["1"] = "Facts file not found."
        @payload["code"] = 1
        @payload["output"] = @output.to_json
        ok @payload.to_json
    end
    
    begin
       facts = File.readlines(FactFile)
       if facts.grep(/^frequency/).length() < 1
          @output["1"] = "Frequency not found"
          @payload["code"] = 1
          @payload["output"] = @output.to_json
          ok @payload.to_json
       end
       freq=facts.grep(/^frequency/)[0].split("=")[1].to_i * 3600
       exit_code=0
       status=[]
       vhots.each do |vhost|
          vh_status=facts.grep(/#{vhost}/)
          if vh_status.length() < 1
             exit_code = 1 unless exit_code > 1
             status << "App not backed up yet: #{vhost}"
          else
             error=vh_status.grep(/error_code/)
             last=vh_status.grep(/last_backup/)[0].split("=")[1].strip()
             if error.length() > 0
                error_code = error[0].split("=")[1].to_i
                exit_code = error_code unless error_code < exit_code
             else
                backup_date=Time.strptime(last,"%d/%m/%Y %H:%M %z").to_i
                expect_date=Time.now.to_i - freq
                diff=(backup_date - expect_date) / 3600
                if expect_date > backup_date
                   exit_code = 1 unless exit_code > 1
                end
             end
             status << "sys_user: #{vhost}"
          end
       end
      @payload["code"] = exit_code
      if exit_code == 0
        @output["#{exit_code}"]= "All Applications backup available"
      else
        @output["#{exit_code}"]= status
      end
      @payload["output"] = @output.to_json
      ok @payload.to_json
    rescue
    end

  end
end
