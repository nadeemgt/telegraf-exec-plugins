#!/usr/bin/env ruby

require 'sensu-plugin/check/cli'
require 'socket'

class CheckMySQLStats < Sensu::Plugin::Check::CLI

  option :scheme,
    :long => "--scheme SCHEME",
    :default => "cg.#{Socket.gethostname}"

  option :maxwarn,
         description: "Percentage of connections upon which we'll issue a warning",
         short: '-w NUMBER',
         long: '--warnnum NUMBER',
         default: 80

  option :maxcrit,
         description: "Percentage of connections upon which we'll issue an alert",
         short: '-c NUMBER',
         long: '--critnum NUMBER',
         default: 90

  option :user,
         description: 'MySQL User',
         short: '-u USER',
         long: '--user USER',
         default: 'sensu'
 
  option :password,
         description: 'MySQL Password',
         short: '-p PASS',
         long: '--password PASS',
         default: "#{Socket.gethostname}"

  # Execute command line mysql query to retrieve stat
  def mysql_query(query)
    result = `mysql --connect-timeout=5 -u #{config[:user]} -p#{config[:password]} -e "#{query}" -BN 2>/dev/null`.split()[1]
    return result.to_i
  rescue => error
    warning error.message
  end

  def run
    max_con = mysql_query("SHOW VARIABLES LIKE 'max_connections'")
    max_used_con = mysql_query("SHOW GLOBAL STATUS LIKE 'max_used_connections'")

    # Invalid max connections
    warning "Max connections = 0, connections possibly full or MySQL down." if max_con == 0
 
    # Calculate percentage of used connections
    pc = (max_used_con.fdiv(max_con) * 100).round(2)

    # Reset stat if value was critical
    mysql_query("flush status;") if pc >= config[:maxcrit].to_i

    critical "#{pc}% MySQL connections used. Max used: #{max_used_con} Limit: #{max_con}" if pc >= config[:maxcrit].to_i
    # warning "#{pc}% MySQL connections used. Max used: #{max_used_con} Limit: #{max_con}" if pc >= config[:maxwarn].to_i
    ok "Max connections used is under limit in MySQL: #{max_used_con} out of #{max_con}" 
  rescue => error
    warning error.message
  end
end
