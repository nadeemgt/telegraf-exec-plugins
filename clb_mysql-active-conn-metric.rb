#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'

class CheckMySQLActiveConn < Sensu::Plugin::Metric::CLI::Graphite
  option :scheme,
    :long => "--scheme SCHEME",
    :default => "cg.#{Socket.gethostname}"

  option :user,
         description: 'MySQL User',
         short: '-u USER',
         long: '--user USER',
         default: 'sensu'
 
  option :password,
         description: 'MySQL Password',
         short: '-p PASS',
         long: '--password PASS',
         default: "#{Socket.gethostname}"

  # Execute command line mysql query to retrieve stat
  def mysql_query(query)
    result = `mysql --connect-timeout=5 -u #{config[:user]} -p#{config[:password]} -e "#{query}" -BN 2>/dev/null`.split()[1]
    
    warning "Mysql error" unless $?.success?
    return result.to_i
  rescue => error
    warning error.message
  end

  def run
    # get active connections and current time
    # Subtract 1 so that the below query connection is not counted.
    active_conn = mysql_query("SHOW STATUS WHERE variable_name = 'Threads_connected';") - 1

    # set connections to 0 if negative
    active_conn = 0 if active_conn < 0
    timestamp = Time.now.to_i

    # ouput data for graphite
    output [config[:scheme], "mysql", "active_conn"].join("."), active_conn, timestamp
    ok
  rescue => error
    warning error.message
  end
end
