#!/usr/bin/env ruby
require 'rubygems' if RUBY_VERSION < '1.9.0'

require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'
require 'digest/md5'

NGINXSITESPATH = "/etc/nginx/sites-available/"
SEARCHPATTERN = "#domain-name-migration:"
DOMAINROUTE = "/cw-domain-migration-"
DOMAINROUTE_NEW = "/cw-domain-migration-arandomsalttomakeitunique-"
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
MIGRATION_MAPS = "/etc/nginx/extras/migration_keys.conf"

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'
MSG_CRITICAL = "Domain is migrated"
MSG_OK = "Domain is not migrated"
MSG_UNKNOWN = "Migration call is not setup"

class DomainMigration < Sensu::Plugin::Metric::CLI::Graphite

  def initialize
    super
    @output={}
    @payload={ "type"=>"alert", "immediate"=>true }
  end

  def run

    list_of_sites = Dir[NGINXSITESPATH + "*"]
    hostname = Socket.gethostname
    resp_critical = {C_FAIL => []}
    resp_ok = {C_SUCCESS => []}
    resp_unknown = {C_UNKNOWN => []}
      
    list_of_sites.each do |item|
      #Getting sys_user from path
      sys_user = item.gsub(NGINXSITESPATH, '')
      domain_name = `grep -io -m1 '#{SEARCHPATTERN}.*' #{item}`
      domain_name = domain_name.to_s.gsub(/#{SEARCHPATTERN}/i, '').chomp("\n")
      hash_url = "-" + Digest::MD5.hexdigest(domain_name + hostname)
      domain_name = domain_name.downcase.strip.sub(/^(www.)/, "")
      id = rand(7777).to_s
      url_request = "#{domain_name}#{DOMAINROUTE}#{sys_user}#{hash_url}/?id=#{id}"

      unless domain_name.empty?

        begin
          cw_delimiter = "CW_DELIMITER"
          curl_command = `curl -k --connect-timeout 15 --max-time 15 -A "#{USER_AGENT}" -Ls  -w #{cw_delimiter}%{http_code} #{url_request}`
          body = curl_command.partition(cw_delimiter).first.strip
          status_code = curl_command.partition(cw_delimiter).last.strip

        rescue
          resp_ok[C_SUCCESS].push({'domain_name' => "#{domain_name}",
                                   'sys_user' => "#{sys_user}", 'message' => "#{MSG_OK}"})
          next
        end

        if status_code == "200" && body == "OK"
          resp_critical[C_FAIL].push({'domain_name' => "#{domain_name}",
                                      'sys_user' => "#{sys_user}", 'message' => "#{MSG_CRITICAL}"})
        else
          resp_ok[C_SUCCESS].push({'domain_name' => "#{domain_name}",
                                   'sys_user' => "#{sys_user}", 'message' => "#{MSG_OK}"})
        end
                      
      end
               
    end


    if File.file?(MIGRATION_MAPS)
      File.open(MIGRATION_MAPS, "r") do |file|
        for line in file.readlines()
          captures = line.match(/.*\<(.*)\>\<(.*)\>/i).captures
          domain_name = captures[0]
          sys_user = captures[1]
          domain_name = domain_name.downcase.strip.sub(/^(www.)/, "")
          id = rand(7777).to_s
          url_request = "#{domain_name}#{DOMAINROUTE_NEW}#{sys_user}/?id=#{id}"

          begin
            cw_delimiter = "CW_DELIMITER"
            curl_command = `curl -k --connect-timeout 15 --max-time 15 -A "#{USER_AGENT}" -Ls  -w #{cw_delimiter}%{http_code} #{url_request}`
            body = curl_command.partition(cw_delimiter).first.strip
            status_code = curl_command.partition(cw_delimiter).last.strip

          rescue
            resp_ok[C_SUCCESS].push({'domain_name' => "#{domain_name}",
                                     'sys_user' => "#{sys_user}", 'message' => "#{MSG_OK}"})
            next
          end

          if status_code == "200" && body == "OK"
            resp_critical[C_FAIL].push({'domain_name' => "#{domain_name}",
                                        'sys_user' => "#{sys_user}", 'message' => "#{MSG_CRITICAL}"})
          else
            resp_ok[C_SUCCESS].push({'domain_name' => "#{domain_name}",
                                     'sys_user' => "#{sys_user}", 'message' => "#{MSG_OK}"})
          end
        end
      end
    end
    
    if resp_critical[C_FAIL].length > 0
      @payload["code"] = 2
      @payload["output"] = resp_critical.to_json
      ok @payload.to_json
    elsif resp_ok[C_SUCCESS].length > 0
      @payload["code"] = 0
      @payload["output"] = resp_ok.to_json
    else
      @payload["code"] = 3
      resp_unknown[C_UNKNOWN].push({'message' => "#{MSG_UNKNOWN}"})
      @payload["output"] = resp_unknown.to_json
    end
    ok @payload.to_json
  end

end