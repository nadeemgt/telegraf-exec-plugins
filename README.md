# README #

CLoudways Test Repository By Nadeem

### What is this repository for? ###

* This Reposistory is in testing state for as inputs.exec plugins for Telegraf

### How do I get set up? ###

* Get a pull in to /etc/telegraf/plugins
* Create a symlink `ln -fsv /etc/telegraf/plugins/telegraf.conf /etc/telegraf/telegraf.conf` (Just for testing)

### Contribution guidelines ###

* Just get hang of ruby to make scripts

### Who do I talk to? ###

* Muhammad Nadeem (nadeem@cloudways.com)
* Backend Team at Cloudways
