#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class ZApc < Sensu::Plugin::Metric::CLI::Graphite

    #option :scheme,
    #:description => "Metric naming scheme, text to prepend to .$parent.$child",
    #:long => "--scheme SCHEME",
    #:default => "#{Socket.gethostname}.zabbix"

    def check(temp)
        if temp.chomp! != "0"
            exit
        end
    end

    def connector_dir
        count = `grep -e "fpm=enable" /etc/ansible/facts.d/packages.fact -c`
        if count.to_i > 0
            return 'fpm'
        else
            verify_fpm = `dpkg -l | grep 'php.*-fpm' | grep '^ii' 1> /dev/null 2> /dev/null; echo $?`
            if verify_fpm.chomp! == "0"
                return 'fpm'
            else
                return 'apache2'
            end
        end
    end

    def phpversion
        phpv = `php -v`
        unless phpv.nil?
            phpv
            .downcase()
            .match(/php\s*(\d\.\d)/)
            .captures.first
        end
    end

    def directoryExists(directory)
        File.directory?(directory)
    end

    def run
        hostname = "#{Socket.gethostname}.zabbix"
        check(`/etc/init.d/apache2 status 2>/dev/null | grep -wq "is running\\|active"; echo $?`)

        basedir=""
        dirs=["/etc/php/#{phpversion}/#{connector_dir}/conf.d",
              "/etc/php5/#{connector_dir}/conf.d",
              "/etc/php/#{connector_dir}/conf.d"]

        dirs.each do |dir|
            if directoryExists(dir)
                basedir=dir            
                break
            end
        end
        
        if basedir.empty?
            exit
        end

        check(`ls #{basedir}/??-ap*.ini 1> /dev/null 2> /dev/null ;echo $?`)
        check(`cat #{basedir}/??-ap*.ini |  grep -q '^extension.*=.*apc*';echo $?`)

        port = `netstat -tunlp |  awk '$7 ~ "apache" && $4 ~ /\:80/' | awk '{print $4}' | awk -F: '{print $NF}' | head -n 1`.chomp!

        if port == nil
            port = 8081
        end
        hitrate = `/usr/bin/php /var/cw/scripts/php/apc-stats.php #{port} | grep hits_rate | cut -d"=" -f2`.chomp!.to_f.round(4)
        fillratio = `/usr/bin/php /var/cw/scripts/php/apc-stats.php #{port} | grep fill_ratio | cut -d"=" -f2`.chomp!.to_f.round(4)
        timestamp = Time.now.to_i
        metrics = {
            :apc => {
                :hitrate => hitrate,
                :fillratio => fillratio
            }
        }
        metrics.each do |parent, children|
            children.each do |child, value,|
                output "zabbix.#{parent},type=metric #{child}=#{value} #{timestamp}000000000"
            end
        end
        ok
    end
end
