#!/usr/bin/env ruby 
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'json'
require 'socket'

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

# FQDN
$fqdn = `hostname`.chomp

$results = { 
  C_SUCCESS => {},
  C_FAIL => {},
  C_UNKNOWN =>{}
}

class AppWordpressSettingChecker < Sensu::Plugin::Metric::CLI::Graphite
  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def initialize
    super
    # grep master user name
    @master_user = `getent passwd | grep '\/home\/master\:' | cut -d: -f1`.chomp()
    @payload={"type"=>"alert", "immediate"=>false, "code"=>0 }
  end

  def run
    # Grep all installed wordpress apps on system
    wp_apps = get_apps 
    if wp_apps.empty?
        @payload["output"] =  {C_SUCCESS => {}}.to_json
    else
        cmd = `echo #{@master_user}`.chomp()
        if cmd == ''
               # if master user not found in the output 
           @payload["code"] = 3
           @payload["output"] = {C_UNKNOWN => {}}.to_json
        else
           wp_apps.each do |app|
                check_app_cache(app)
           end
           if not $results[C_UNKNOWN].empty?
                @payload["code"] = 3
           elsif not $results[C_FAIL].empty?
                @payload["code"] = 2
           else 
                @payload["code"] = 0
           end
           @payload["output"] = $results.to_json
        end
    end
    ok @payload.to_json
  rescue => error
    @payload["code"] = 1
    @payload["ouptut"] = {"1"=>error.message}.to_json
    ok @payload.to_json
  end
end

def check_app_cache(app)
  app_settings_dict = {}

  app_root = get_app_root app

  status = C_SUCCESS
 
  plugin, status = cache_enabled?(app, app_root)
  if plugin == "breeze"
    status, app_settings_dict["breeze"] = check_breeze_settings(app, app_root)

  elsif plugin != ''
    app_settings_dict[plugin] = {}

    if status
      status = C_SUCCESS
    else
      status = C_FAIL
    end

  else
    status = C_FAIL
    app_settings_dict[plugin] = {}
  end

  $results[status][app] = app_settings_dict
end

# Checks if cache enabled
# Return <plugin_name>, <active [true|false]>
def cache_enabled?(app, app_root)
  plugins = ["breeze", "w3-total-cache", "wp-rocket"]

  plugins.each do |plugin|
    output = `su #{@master_user} -c "cd #{app_root}/ && wp plugin is-active #{plugin} 2>/dev/null"`
    status_code = $?.exitstatus

    if status_code == 0
      return plugin, true
    end
  end

  return '', false
end

def check_breeze_settings(app, app_root)
  conf_file = "#{app_root}/wp-content/breeze-config/breeze-config.php"

  # if file doesn't exist then it's not activated
  if not File.exists?(conf_file)
    status = C_FAIL
    breeze_active = '0'
  else
    # Sample line: 'breeze-active' => '1',
    match = File.foreach(conf_file).grep(/breeze-active/)
    # Match found
    if match.count > 0
      breeze_active = match[0].split(' => ')[1].tr('\', ', '').chomp();

      if breeze_active == '1'
        status = C_SUCCESS
      else 
        status = C_FAIL
      end
    else
      status = C_UNKNOWN
      breeze_active = '0'
    end
  end

  cw_setting = { 'breeze-active' => { "enabled" => breeze_active} } 
  return status, cw_setting
rescue => error
  status = C_UNKNOWN
  cw_setting = { 'breeze-active' => { "enabled" => error.message } }
  return status, cw_setting
end

def get_app_root(app)
  v_host_file = "/etc/nginx/sites-available/#{app}"
  _app_root = open(v_host_file) { 
    |f| f.grep(/root/) 
  }
  app_root = _app_root[0].split()[1].tr(';', '')
rescue
  return "/home/#{$fqdn}/#{app}/public_html"
end

def get_apps()
  wp_to_check = ["wordpress", "woocommerce", "wordpressmu"]

  # Get apps from nginx vhost to prevent duplicates because of symlinks
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  wp_apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      catch :app_found do
        file.each_line do |line|
          wp_to_check.each do |wp|
            if line =~ /#{wp}/
              wp_apps << vhost
              throw :app_found 
            end
          end
        end 
      end
    end 
  end 
  return wp_apps
end
