#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'json'
require 'socket'
require 'net/http'
require 'uri'

S3_FACT_URL = "https://cloudways-static-content.s3.amazonaws.com/applications/magento/latest.txt"

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'
C_WARNING = '1'

APP_TO_CHECK="magento"
FQDN = Socket.gethostname

class AppMagentoVersionChecker < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def initialize
    super
    @crit = []
    @unknown = []
    @warning = []
    @output = {}
    @payload = {"type"=>"alert", "immediate"=>true, "code"=>0}
  end

  def fetch_latest_version
    uri = URI.parse(S3_FACT_URL)
    response = Net::HTTP.get_response(uri)

    if response.code.to_i != 200
      @payload["code"]=1
      @payload["output"]="Error #{response.code} while fetching latest version"
      ok @payload.to_json
    end
    return response.body.strip
  end

  def run
    # Get all magento app names
    magento_apps = get_apps
    if magento_apps.empty?
      @payload["code"]=0
      @output = {C_SUCCESS => {}}.to_json
    else
      # Get versions of apps
      magento_apps_dict = get_app_versions magento_apps
      if magento_apps_dict.empty?
        @payload["code"] = 0
        @output = {C_SUCCESS => {}}.to_json
      else
        latest_version = fetch_latest_version
        magento_apps_dict.each do |app, version|
          case version_compare(version,latest_version)
            when "older"
              # @crit << "App #{app} version: #{version}, latest: #{latest_version}"
              insert_payload(C_FAIL, app, version, latest_version)
            when "upToDate"
              insert_payload(C_SUCCESS, app, version, latest_version)
            else
              # @warning << "App #{app} version: #{version}, latest: #{latest_version}"
              insert_payload(C_WARNING, app, version, latest_version)
          end
        end
      end
    end
    # warning ({ "1" => @warning }.to_json) unless @warning.empty?
    # critical @payload.to_json unless @crit.empty?
    @payload["output"] = @output.to_json
    ok @payload.to_json
  rescue => error
    @payload["output"] = error.message
    @payload["code"] = 1
  end

  def insert_payload(status, app, version, latest)
    @payload["code"] = status.to_i unless @payload["code"] > status.to_i or @payload["code"] == 1
    @output[status] = {} unless @output.key?(status)
    @output[status][app] = { "latest_version"=> latest, "current_version"=> version }
    # @payload[status] = {} unless @payload.key?(status)
    # @payload[status][app] = { "latest_version"=> latest, "current_version"=> version }
  end

  def version_compare(current,latest)
    @cur = current.split('.').map {|s| s.to_i}
    @lat = latest.split('.').map {|s| s.to_i}
    @lat.zip(@cur).each do |l,c|
      if l > c 
        return "older"
      elsif l  < c 
        return "newer"
      end
    end
    return "upToDate"
  end


  def get_app_root(app)
    v_host_file = "/etc/nginx/sites-available/#{app}"
    _app_root = open(v_host_file) {
      |f| f.grep(/root/)
    }
    app_root = _app_root[0].split()[1].tr(';', '')
  rescue
    return "/home/#{FQDN}/#{app}/public_html"
  end

  def get_bin_dir(app_root)
      Dir.chdir(app_root)
      if Dir.entries('.').include? "bin"
        Dir.chdir('./bin/')
        return Dir.pwd
      elsif Dir.entries("..").include? "bin"
        Dir.chdir('../bin/')
        return Dir.pwd
      else
        return nil
      end
  end

  def get_apps()
    # Get apps from nginx vhost to prevent duplicates because of symlinks
    vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}
    apps = []
    vhosts.each do |vhost|
      next unless File.exists?("/home/#{FQDN}/#{vhost}/conf/server.nginx")
      File.open "/home/#{FQDN}/#{vhost}/conf/server.nginx" do |file|
          file.each_line do |line|
              if line =~ /#{APP_TO_CHECK}/
                apps << vhost
                break
              end
          end
      end
    end
    return apps
  end

  # Returns a dict of magento apps and thier versions
  def get_app_versions(magento_apps)
    magento_apps_dict = {}
    # Check each apps version using Mage
    magento_apps.each do |app|
      app_root = get_app_root app
      # Look for magento 2.X installations
      bin_dir = get_bin_dir(app_root)
      if (not bin_dir.nil?) && File.exists?("#{bin_dir}/magento")
        output = `sudo -u #{app} php #{bin_dir}/magento -V --no-ansi`
        if $?.exitstatus != 0
                @warning << "#{app}: #{output}"
                next
        end
        magento_current = output.split(" ")[-1]
        magento_apps_dict[app] = magento_current      
      elsif File.exists?("#{app_root}/app/Mage.php")
        # skip Magento 1 due to EOS EOL
        next
      else
        # If magento version unknown, because mage/cli not found
        magento_apps_dict[app] = "UNKNOWN"
      end
    end
    return magento_apps_dict
  end
end
