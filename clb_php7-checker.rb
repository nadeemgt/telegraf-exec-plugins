#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli' 
require 'json' 
require 'socket' 

# Codes 
C_SUCCESS = '0' 
C_FAIL = '2' 
C_UNKNOWN = '3' 

FACTS_FILE = '/etc/ansible/facts.d/packages.fact' 

# FQDN
$fqdn = `hostname`.chomp

class AppWordpressPhp7 < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def initialize
    super
    @warn = {}
    @crit = {}
    @output = {}
    @payload = {"type"=>"alert", "immediate"=>true }
  end


  def run

    # php 7 not installable on wheezy
    if get_os_info()['Codename'] == 'wheezy'
      @payload["code"] = 0
      @output[C_SUCCESS] = "Wheezy: PHP7 not installable"
    else
      # Grep all installed wordpress apps on system
      wp_apps = get_apps
      if wp_apps.empty?
        @payload["code"] = 0
        @output[C_SUCCESS] = "No WP apps found"
      elsif get_php_version().start_with?('7')
        # php 7 already installed
        @payload["code"] = 0
        @output[C_SUCCESS] = "PHP7 installed"
      else
        @payload["code"] = 0
        @output[C_SUCCESS] = "PHP7 not installed"
      end
    end
    @payload["output"] = @output.to_json
    ok @payload.to_json
  rescue => error
    warning error.message
  end
end

  def get_apps()
    wp_to_check = ["wordpress", "woocommerce", "wordpressmu"]
  
    # Get apps from nginx vhost to prevent duplicates because of symlinks
    vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}
  
    wp_apps = []
    vhosts.each do |vhost|
      File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
        file.each_line do |line|
          wp_to_check.each do |wp|
            if line =~ /#{wp}/
              wp_apps << vhost
            end
          end
        end
      end
    end
    return wp_apps
  end
  
  def get_os_info()
    info = {}
    `lsb_release -a 2> /dev/null`.each_line do |line|
      if line.include?(':')
        key, val = line.split(':')
        info[key] = val.chomp().strip()
      end
    end
    return info
  end
  
  def get_php_version()
    return File.open(FACTS_FILE).grep(/php/)[0].chomp().split('=')[1]
  end
