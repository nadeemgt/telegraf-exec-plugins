#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'date'
require 'sensu-plugin/metric/cli'
require 'socket'

base_log_path = '/var/cw/stats/sensu' 
LOG_FILE = File.join(base_log_path, 'cpu_metrics.log')

# Dir.mkdir(base_log_path) unless File.exists?(base_log_path)


class CpuGraphite < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to metric",
    :short => "-s SCHEME",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}.cpu"

  option :metrics_list,
    :description => "Comma-separated metrics list",
    :short => "-m METRICS",
    :long => "--metrics-list METRICS",
    :default => "user, nice, system, idle, iowait, irq, softirq, steal, guest"

  def get_proc_stats
    cpu_metrics = ['user', 'nice', 'system', 'idle', 'iowait', 'irq', 'softirq', 'steal', 'guest']
    File.open("/proc/stat", "r").each_line do |line|
      info = line.split(/\s+/)
      name = info.shift

      # we are matching TOTAL stats and returning a hash of values
      if name.match(/^cpu$/)
        # return the CPU metrics sample as a hash
        return Hash[cpu_metrics.zip(info.map(&:to_i))]
      end
    end
  end

  def sum_cpu_metrics(metrics)
    metrics.values.inject {|sum, metric| sum+metric}
  end

  # Special case for KYUP
  def confirm_idle_metric(idle)
    if ! File.file?('/var/spool/cpu.util')
      # Not KYUP, so return metric as is        
      return idle
    else
      # KYUP, so read cpu usage value from /var/spool/cpu.util
      # subtract 100 to get idle value and return
      new_idle = sprintf('%.2f', (100 - File.read('/var/spool/cpu.util').to_f)).to_f
      new_idle >=0 ? new_idle : 0
    end
  end

  def run
    cpu_metrics = ['user', 'nice', 'system', 'idle', 'iowait', 'irq', 'softirq', 'steal', 'guest']
    cpu_sample1 = get_proc_stats
    sleep(1)
    cpu_sample2 = get_proc_stats

    # we will sum all jiffy counts read in get_proc_stats
    cpu_total1 = sum_cpu_metrics(cpu_sample1)
    cpu_total2 = sum_cpu_metrics(cpu_sample2)
    # total cpu usage in last second in CPU jiffs (1/100 s)
    cpu_total_diff  = cpu_total2 - cpu_total1
    # per CPU metric diff
    cpu_sample_diff = Hash[cpu_sample2.map { |k, v| [k, v - cpu_sample1[k]] }]

    custom_metrics = config[:metrics_list].split(',').map { |el| el.strip() }
    cpu_metrics.each do |metric|
                        metric_val = sprintf("%.02f", (cpu_sample_diff[metric]/cpu_total_diff.to_f)*100)

                        if metric == 'idle'
                          metric_val = confirm_idle_metric(metric_val)
                          write_to_file('cpu.idle', metric_val)
                        end
                        if custom_metrics.include?(metric)
                                output "#{config[:scheme]}.#{metric}", metric_val
                        end
    end
    ok
  end

  def write_to_file(field, data)
    time = DateTime.now.strftime('%d/%b/%Y:%H:%M:%S %z')
    line = "[#{time}] #{field} #{data}\n"
    File.open(LOG_FILE, "a") do |f|
      f.write line
    end
  rescue
    return 
  end
end
