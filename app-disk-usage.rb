#!/usr/bin/env ruby
require 'timeout'
require 'socket'
require 'sensu-plugin/metric/cli'

class AppSizeMetrics < Sensu::Plugin::Metric::CLI::Influxdb #Graphite


  option :ignore_mnt,
    :description => 'Ignore mounts matching pattern(s)',
    :short => '-i MNT[,MNT]',
    :long => '--ignore-mount',
    :proc => proc { |a| a.split(',') }

  option :include_mnt,
    :description => 'Include only mounts matching pattern(s)',
    :short => '-I MNT[,MNT]',
    :long => '--include-mount',
    :proc => proc { |a| a.split(',') }

  option :flatten,
    :description => 'Output mounts with underscore rather than dot',
    :short => '-f',
    :long => '--flatten',
    :boolean => true,
    :default => false

  option :scheme,
    description: 'Metric naming scheme, text to prepend to metric',
    short: '-s SCHEME',
    long: '--scheme SCHEME',
    required: true,
    default: "cg.#{Socket.gethostname}.disk"

  option :user,
    description: 'get app+mysql size for specific user',
    short: '-u USER',
    long: '--user'

  option :db,
    description: 'get mysql size for specific user',
    short: '-d DATABASE',
    long: '--db'

  option :data,
    description: 'get app size for specific user',
    short: '-data DATA',
    long: '--data'

  option :mysql_user,
    description: 'MySQL User',
    short: '-m MYSQLUSER',
    long: '--mysql MYSQLUSER',
    default: 'sensu'

  option :password,
    description: 'MySQL Password',
    short: '-p PASS',
    long: '--password PASS',
    default: "#{Socket.gethostname}"


  def run
    if config[:db]
      data_size = app_size("#{config[:db]}").to_f
      status = $?
      if status == 0
        db_size = mysql_size("#{config[:db]}")
        output "#{config[:scheme]}.#{config[:db]}.db", db_size
      else
        critical "user does not exists"
      end
      exit;
    end

    if config[:data]
      data_size = app_size("#{config[:data]}").to_f
      status = $?
        if status == 0
          output "#{config[:scheme]}.#{config[:data]}.data", data_size
        else
          critical "user does not exist"
        end
      exit;
    end

    # Specific app user size get
    if config[:user]
      data_size = app_size("#{config[:user]}").to_f
      status_code = $?
        if status_code == 0
          output "#{config[:scheme]}.#{config[:user]}.data", data_size
          db_size = mysql_size("#{config[:user]}")
          output "#{config[:scheme]}.#{config[:user]}.db", db_size
        else
          critical "user does not exist"
        end

    else
      users = `ls /etc/nginx/sites-available/`.split
      for i in users do
        sum = app_size(i)
        if sum and sum > 0
          sum += mysql_size(i)
          if $? == 0
		output "#{config[:scheme]}.apps", "type=metric", "#{i}=#{sum}"
		#output "#{config[:scheme]}.apps.#{i}", sum
          end
        end
      end

      delim = config[:flatten] == true ? '_' : '.'
      # Get disk usage from df with used in megabytes
      `df -PBM`.split("\n").drop(1).each do |line|
        _, _, used, avail, used_prct,  mnt = line.split

        unless %r{/sys|/dev|/run|/tmp|/proc}.match(mnt)
          next if config[:ignore_mnt] && config[:ignore_mnt].find { |x| mnt.match(x) }
          next if config[:include_mnt] && !config[:include_mnt].find { |x| mnt.match(x) }
          if config[:flatten]
            mnt = mnt.eql?('/') ? 'misc' : mnt.gsub(/^\//, '')
          else
            mnt = mnt.length == 1 ? 'misc' : mnt.gsub(/^\//, 'misc.')
          end
          # Fix subsequent slashes
          mnt = mnt.gsub '/', delim
          output [config[:scheme], mnt ].join('.'),"type=metric" , "used=#{used.gsub('M', '')}"
        end
      end
    end
    ok
  end

  def get_mysql_timeout
    facts_file = '/etc/ansible/facts.d/sensu_setting.fact'
    mysql_timeout = File.readlines(facts_file).grep(/^mysql_timeout/)[0].split('=')[1].chomp()
    return mysql_timeout
  rescue
    return '30'
  end  

  def mysql_query(query)
    Timeout::timeout(get_mysql_timeout().to_i) do
      result = `mysql --connect-timeout=5 -u #{config[:mysql_user]} -p#{config[:password]} -e "#{query}" -BN 2>/dev/null`
      warning "Mysql error" unless $?.success?
      return result
    end
  rescue => error
    warning error.message
  end


  def mysql_size(sys_user)
    mysql_query("SELECT sum( data_length + index_length) / 1024 / 1024    
        FROM information_schema.TABLES where table_schema ='#{sys_user}' group by table_schema;").to_f
  end


  #to fetch sys_user ( application files ) size
  def app_size(sys_user)
    if is_valid_user(sys_user)
      cmd = "/usr/bin/du /home/#{Socket.gethostname}/#{sys_user} --summarize -k 2>/dev/null"
      `#{cmd}`.split()[0].to_f / 1024
    end
  end


  #verify sys user is valid
  def is_valid_user(sys_user)
    if sys_user and sys_user != ""
      home_dir = File.join("/home", Socket.gethostname, sys_user)
      return File.directory?(home_dir)
    end

    return false
  end

end

