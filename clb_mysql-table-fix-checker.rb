#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'

$facts_file = "/etc/ansible/facts.d/mysql_crashed.fact"

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

# FQDN
$fqdn=`hostname`.chomp

class CheckMySQLTableFix < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :long => "--scheme SCHEME",
    :default => "cg.#{Socket.gethostname}"

  def initialize
    super
    @output = {}
    @payload = {"type"=>"alert", "immediate"=>true, "code"=>0, "output"=>""}
  end

  def run 
    # Quit if facts file doesn't exist
    if ! File.file?($facts_file)
      @payload["code"] = 0
      @payload["output"] = "Facts file not found" 
    else
      # Read facts file line by line
      File.foreach($facts_file) do |line|
        if line =~ /db_[a-z]+=/
            _db, _tables = line.split('=') 
            db = _db.split('_')[1]
            tables = _tables.chomp().split(',') 
            insert_payload(C_FAIL, db, tables) 
        end
      end 

      if @output.has_key?(C_FAIL)
        @payload["code"] = 2
        @payload["output"] = @output.to_json
      else
        @payload["code"] = 0
        @payload["output"] = {C_SUCCESS => {}}.to_json
      end
    end
    ok @payload.to_json
  rescue => error
    @payload["code"] = 1
    @payload["output"] = error.message
    ok @payload.to_json
  end 

  def insert_payload(status, app, data)
    @output[status] = {} unless @payload.key?(status)
    @output[status][app] = data 
  end
end
