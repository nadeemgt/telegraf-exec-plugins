#!/usr/bin/env ruby
require 'sensu-plugin/metric/cli' 

require "date"
require 'json' 
require 'time'
require 'socket' 

# Codes 
C_SUCCESS = '0' 
C_FAIL = '2' 
C_UNKNOWN = '3' 

MAIL_LOG_FILE = "/var/log/mail.log"

SMTP_FILE = "/etc/postfix/main.cf"

CHECK = {
  "failed_logs" => 0,
  "smtp_enabled" => "NO"
}

class PostfixSmtp < Sensu::Plugin::Metric::CLI::Graphite
  option :time,
    :description => "Time limit for which erros are checked",
    :long => "--time TIME",
    :proc => proc(&:to_i),
    :default => 24 #24 hours

  option :threshold,
    :description => "The LOWER limit errors in error logs.",
    :short => "-t THRESHOLD",
    :long => "--threshold THRESHOLD",
    :proc => proc(&:to_i),
    :default => 30 #30 errors in log

  def initialize
    super
    @warn = {}
    @crit = {}
    @output = {}
    @payload = { "type"=>"alert", "immediate"=>true, "code"=>0 }
  end


  def run
    begin 
      self.checkMailLog
      self.checkSmtpSettings

      #if no error in Mail Logs then check MailQ 
      if CHECK['failed_logs'] == 0
        self.checkMailQ
      end
      output = "Number of Errors: #{CHECK['failed_logs']}"+" SMTP: #{CHECK['smtp_enabled']}"
      result = output

      #critical output if mail error logs are greater than threshold
      if CHECK['failed_logs'] > config[:threshold]
        @payload["code"] = C_FAIL.to_i
	@output["output"] = result
        # @crit[C_FAIL] = result
        # critical @crit.to_json
      else
	@payload["code"] = C_SUCCESS.to_i
        @output[C_SUCCESS] = output
        # ok @payload.to_json
      end
      @payload["output"] = @output.to_json
      ok @payload.to_json
    rescue => error
      @payload["code"] = C_UNKNOWN.to_i
      @output[C_UNKNOWN] = warning error.message
      @payload["output"] = @output.to_json
      ok @payload.to_json
    end
  end

  #this function checks for error logs in mailq
  def checkMailQ
    numLogs = 0
    begin
      if File::exists?( SMTP_FILE )
        mailq_output = `mailq`.split("\n")
        mailq_output.each do |line|
          if line.match("cloudwaysapps.com")
            time = line.split(" ")
            total = Time.now() - Time.parse(time[3]+" "+time[4]+" "+time[5])
            factor = 3600 * config[:time]
            days = total / factor
            if days < 1  
              numLogs += 1
            end 
          end
        end
        if numLogs > 0
          CHECK['failed_logs'] = numLogs;
        end
      else
      end
    rescue
    end
  end

  #check for errors in mail log
  def checkMailLog
    mail_log_true_string = "status=sent"
    mail_log_check_string = "status"
    if File::exists?( MAIL_LOG_FILE ) 
      numLogs=0
      File.open( MAIL_LOG_FILE ).each_line do |line| 
        if line.match( mail_log_true_string )
        else 
          if line.match( mail_log_check_string )
            line.chomp!
            begin 
              time = line.split(" ")
              total = Time.now() - Time.parse(time[0]+" "+time[1]+" "+time[2])
              factor = 3600 * config[:time]
              days = total / factor
              if days < 1  
                numLogs += 1
              end  
            rescue
            end
          end
        end
        if numLogs > 0
          CHECK['failed_logs'] = numLogs;
        end
      end
    else
      CHECK['failed_logs'] = 0;
    end
  end

  #this function checks for SMTP settings
  def checkSmtpSettings
    smtp_string = "smtp_use_tls= yes"
    if File::exists?( SMTP_FILE )
      smtp = 0
      File.open( SMTP_FILE ).each_line do |line|
        line.chomp!
        begin 
          if line.match( smtp_string )
            smtp =  1;
          end
        rescue
        end
      end
      if smtp == 1
        CHECK['smtp_enabled'] = "YES"
      end
    else
      CHECK['smtp_enabled'] = "NO"
    end
  end 
end
