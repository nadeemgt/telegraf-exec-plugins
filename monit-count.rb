#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class Monit < Sensu::Plugin::Metric::CLI::Graphite

#  option :scheme,
#    :description => "Metric naming scheme, text to prepend to .$parent.$child",
#    :long => "--scheme SCHEME",
#    :default => "#{Socket.gethostname}.monit"

        def run
        hostname = "#{Socket.gethostname}.monit"
        datem = `date +%b`.chomp
        dated = `date +%-d`.chomp

        number = `cat /var/log/monit.log | grep "#{datem} *#{dated}" | grep start: | wc -l`.chomp
                timestamp = Time.now.to_i

        metrics = {
                :event => {
                        :count => number
                }
        }
        metrics.each do |parent, children|
        children.each do |child, value,|
        output "monit.#{parent},type=metric #{child}=#{value} #{timestamp}"
      end
    end
    ok
  end
end
