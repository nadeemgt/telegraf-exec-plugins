#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'date'
require 'sensu-plugin/metric/cli'
require 'socket'

base_log_path = '/var/cw/stats/sensu' 
LOG_FILE = File.join(base_log_path, 'mem_metrics.log')

# Dir.mkdir(base_log_path) unless File.exists?(base_log_path)

class MemoryGraphite < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to metric",
    :short => "-s SCHEME",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}.memory"

  option :metrics_list,
    :description => "Comma-separated list of custom metrics",
    :short => "-m METRICS",
    :long => "--metrics_list METRICS",
    :required => false

  option :metrics_print,
    :description => "Comma-separated list of custom metrics",
    :short => "-p METRICS",
    :long => "--metrics_print METRICS",
    :required => false

  def run
    # Metrics borrowed from hoardd: https://github.com/coredump/hoardd

                if config.has_key?(:metrics_list)
                        metrics_list = config[:metrics_list].split(',').map { |el| el.strip }
                else
                        metrics_list = []
                end

                if config.has_key?(:metrics_print)
                        metrics_print = config[:metrics_print].split(',').map { |el1| el1.strip }
                else
                        metrics_print = metrics_list
                end

    mem = {}
    File.open("/proc/meminfo", "r").each_line do |line|
                        mem['total']     = line.split(/\s+/)[1].to_i * 1024 if line.match(/^MemTotal/)
      mem['free']      = line.split(/\s+/)[1].to_i * 1024 if line.match(/^MemFree/)
      mem['buffers']   = line.split(/\s+/)[1].to_i * 1024 if line.match(/^Buffers/)
      mem['cached']    = line.split(/\s+/)[1].to_i * 1024 if line.match(/^Cached/)
      mem['swapTotal'] = line.split(/\s+/)[1].to_i * 1024 if line.match(/^SwapTotal/)
      mem['swapFree']  = line.split(/\s+/)[1].to_i * 1024 if line.match(/^SwapFree/)
      mem['SReclaimable']  = line.split(/\s+/)[1].to_i * 1024 if line.match(/^SReclaimable/)
    end

    mem['swapUsed'] = mem['swapTotal'] - mem['swapFree']
    mem['used'] = mem['total'] - mem['free']
    mem['usedWOBuffersCaches'] = mem['used'] - (mem['buffers'] + mem['cached'])
    begin
      mem['freeWOBuffersCaches'] = mem['free'] + (mem['buffers'] + mem['cached'] + mem['SReclaimable'])
    rescue
      mem['freeWOBuffersCaches'] = mem['free'] + (mem['buffers'] + mem['cached'])
    end

    # Write mem stats to file for upgrade suggestion check
    write_to_file('memory.free', mem['freeWOBuffersCaches']) if mem.key?('freeWOBuffersCaches')

    mem.each do |k, v|
                        if not metrics_list.empty?
                                if metrics_list.include?(k)
                                        output "#{config[:scheme]}.#{metrics_print.join}", v
                                end
                        else
                                output "#{config[:scheme]}.#{k}", v
                        end
    end

    ok
  end


  def write_to_file(field, data)
    time = DateTime.now.strftime('%d/%b/%Y:%H:%M:%S %z')
    line = "[#{time}] #{field} #{data}\n"
    File.open(LOG_FILE, "a") do |f|
      f.write line
    end
  rescue
    return 
  end
end
