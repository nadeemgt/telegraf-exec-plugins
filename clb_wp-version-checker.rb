#!/usr/bin/env ruby
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'json'
require 'socket'
require 'net/http'
require 'uri'

# Wordpress Version check url
WP_VERSION_URL = "https://s3.amazonaws.com/cloudways-static-content/applications/wordpress/version/latest.txt"

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

NOT_FOUND = "Version not found"

# FQDN
$fqdn = `hostname`.chomp

class AppWordpressChecker < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def initialize
    super
    @crit = []
    @warning = []
    @output = {}
    @payload = { "type"=>"alert", "immediate"=>true, "code"=>0 }
  end

  def run
    # Grep all installed wordpress apps on system
    wp_apps = get_apps
    if wp_apps.empty?
    	@payload["code"] = C_SUCCESS.to_i
    	output = {C_SUCCESS =>{} }
    else  
    	# Fetch latest Wordpress latest version
    	latest_wp_version = fetch_latest_version
  
    	# Get versions of all installed wordpress apps
    	wp_apps_dict = get_app_versions wp_apps

	wp_apps_dict.each do |app, version|
      		if latest_wp_version > version
        		#@crit << "'#{app} version: #{version}, Latest Version: #{latest_wp_version}';"
        		insert_payload(C_FAIL, app, version, latest_wp_version)
      		elsif version == NOT_FOUND
        		#@warning << "'#{app} version: #{version}, Latest Version: #{latest_wp_version}';"
        		insert_payload(C_UNKNOWN, app, NOT_FOUND, latest_wp_version)
      		elsif latest_wp_version < version
        		#@warning << "'#{app} version: #{version}, Latest Version: #{latest_wp_version}';"
        		insert_payload(C_SUCCESS, app, version, latest_wp_version)
      		else
        		insert_payload(C_SUCCESS, app, version, latest_wp_version)
      		end
   	 end

         @payload["output"] = @output.to_json
    	 ok @payload.to_json
    end
  rescue => error
    @payload["code"] = 1
    @payload["output"] = "{\"1\": \"#{warning error.message}\"}"
    ok payload.to_json
  end

  def insert_payload(status, app, version, latest)
    @payload["code"] = status.to_i unless @payload["code"] > status.to_i
    @output[status] = {} unless @output.key?(status)
    #@output["latest_version"] = latest unless @output.key?("latest_version")
    #@output[status][app] = version
    @output[status][app] = {
                              "latest_version"=> latest,
                              "current_version"=> version
                            }
  end
  
  def fetch_latest_version
    uri = URI.parse(WP_VERSION_URL)
    response = Net::HTTP.get_response(uri)

    if response.code.to_i != 200
      warning "Error #{response.code} while fetching latest version"
    end
    return response.body.chomp
  end
end

def get_app_versions(apps)
  wp_version_dict = {}
  apps.each do |app|
    begin
      app_root = get_app_root app

      # CHeck each app version using Wordpress CLI
      app_version = `cd #{app_root}/; \
      wp core version --allow-root 2> /dev/null`.strip()

      app_version = NOT_FOUND if app_version == ""

      wp_version_dict[app] = app_version
    end
  end
  
  return wp_version_dict
end

def get_app_root(app)
  v_host_file = "/etc/nginx/sites-available/#{app}"
  _app_root = open(v_host_file) { 
    |f| f.grep(/root/) 
  }
  app_root = _app_root[0].split()[1].tr(';', '')
rescue
  return "/home/#{$fqdn}/#{app}/public_html"
end

def get_apps()
  wp_to_check = ["wordpress", "woocommerce", "wordpressmu"]

  # Get apps from nginx vhost to prevent duplicates because of symlinks
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  wp_apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      catch :app_found do
        file.each_line do |line|
          wp_to_check.each do |wp|
            if line =~ /#{wp}/
              wp_apps << vhost
              throw :app_found 
            end
          end
        end 
      end
    end 
  end 
  return wp_apps
end

