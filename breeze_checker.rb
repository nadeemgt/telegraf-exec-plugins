#!/usr/bin/env ruby 
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli' 
require 'json'
require 'socket'

# FQDN
$fqdn = `hostname`.chomp

class AppWordpressCacheChecker < Sensu::Plugin::Check::CLI
  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  def run
    # Grep all installed wordpress apps on system
    wp_apps = get_apps 
    ok "No apps found" if wp_apps.empty?

    results = []
    wp_apps.each do |app|
      results << check_app_cache(app)
    end
   
    warning results.join("\n")
  rescue => error
    warning "Error: #{error}"
  end
end

def check_app_cache(app)
  app_root = get_app_root app

  data = File.open("/home/#{$fqdn}/#{app}/conf/server.nginx").grep(/server_name/)[0].tr(';', '').split()

  # get app-fqdn
  app_fqdn = data[1]

  plugin, status = cache_enabled?(app, app_root)

  active = status ? "true" : "false"

  # NAjmus format
  # sys_user, app_fqdn, plugin
  return "#{app},#{app_fqdn},#{plugin}"
end

# Checks if cache enabled
# Return <plugin_name>, <active [true|false]>
def cache_enabled?(app, app_root)
  plugins = ["breeze", "w3-total-cache", "wp-rocket"]
  master_user = `getent passwd | grep master_ | cut -d: -f1`.chomp()

  plugins.each do |plugin|
    output = `su #{master_user} -c "cd #{app_root}/ && wp plugin is-active #{plugin} 2>/dev/null"`
    status_code = $?.exitstatus

    if status_code == 0
      return plugin, true
    end
  end

  return 'None', false
end

def get_app_root(app)
  v_host_file = "/etc/nginx/sites-available/#{app}"
  _app_root = open(v_host_file) { 
    |f| f.grep(/root/) 
  }
  app_root = _app_root[0].split()[1].tr(';', '')
rescue
  return "/home/#{$fqdn}/#{app}/public_html"
end

def get_apps()
  wp_to_check = ["wordpress", "woocommerce", "wordpressmu"]

  # Get apps from nginx vhost to prevent duplicates because of symlinks
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  wp_apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      catch :app_found do
        file.each_line do |line|
          wp_to_check.each do |wp|
            if line =~ /#{wp}/
              wp_apps << vhost
              throw :app_found 
            end
          end
        end 
      end
    end 
  end 
  return wp_apps
end
