#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli' 

require "date"
require 'json' 
require 'socket' 

# Codes 
C_SUCCESS = '0' 
C_WARN = '1'
C_FAIL = '2' 
C_UNKNOWN = '3' 

BASE_LOG_PATH = "/var/cw/stats/sensu"

# Checks 
CHECK = {
  "cpu.idle" => "cpu_metrics.log",
  "memory.free" => "mem_metrics.log"
}

class ServerUpgrade < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  option :check,
    :description => "Metric to check. Options #{CHECK.keys.join('|')}",
    :short => "-c CHECK",
    :long => "--check CHECK",
    :default => "cpu.idle"

  option :threshold,
    :description => "The LOWER limit for an alert. Unit will depend on the metric. cpu->%, memory->MB",
    :short => "-t THRESHOLD",
    :long => "--threshold THRESHOLD",
    :proc => proc(&:to_i),
    :default => 30 #30%

  option :time,
    :description => "Time interval to check in hours. Default: 24 (hours). Other entries will be deleted",
    :long => "--time TIME",
    :proc => proc(&:to_i),
    :default => 24 #24 hours

  def initialize
    super
    @warn = {}
    @crit = {}
    @payload = {}
  end


  # Process log line
  # Extract date, value
  # Return value if in given range, else False
  # Return True if line should be trimmed, else False
  def handle_line(l, check)
    # if check in line...
    if l =~ /#{check}/
      # Sample line
      # [08/Feb/2017:10:17:25 +0000] cpu.idle 89.00
      regex = /\[(.*)\] \S+ (\S+)/
      date, value = l.match(regex).captures

      entry_time = DateTime.strptime(date, '%d/%b/%Y:%H:%M:%S %z')

      current_time = DateTime.now

      if entry_time > current_time - Rational(config[:time], 24)
        # Return value, and false for DO NO TRIM
        return value.to_f, false
      else
        # Return false if old data point, and false for DO TRIM
        return false, true
      end
    else
      # Line not matched, meaning no value, so just TRIM
      return false, true
    end 
  end

  # Return data points that are in the given interval
  # Also return new log file contents, having trimeed old entries
  def get_data_points(log, check)
    points = []
    lines_to_keep = []
    File.foreach(log) do |l|
      begin
        value, trim = handle_line(l, check)
      rescue => error
        value = false
        trim = true
      end

      # Add point if date is in range
      points << value if value

      # Add line for rewriting file if date in range
      lines_to_keep << l unless trim
    end
    return points, lines_to_keep
  end

  def trim_log_file(new_log_file)
    # Rewrite log file without trimmed lines
    File.open(File.join(BASE_LOG_PATH, CHECK[config[:check]]), 'w') do |f|
      new_log_file.each do |line|
        f.write line
      end
    end
  rescue
    return 
  end

  def run
    @warn[C_WARN] = "Invalid metric. Options #{CHECK.keys.join('|')}" 
    warning @warn.to_json unless CHECK.keys.include?(config[:check])

    points, new_log_file = get_data_points(File.join(BASE_LOG_PATH, CHECK[config[:check]]),
                                           config[:check],
                                          )

    # REmove all entries not in specified time range
    trim_log_file(new_log_file)
    if points.empty?
      @warn[C_WARN] = "No points found"
      warning @warn.to_json
    end
    #warning "No points found" if points.empty?
    if points.count <= 24
      @warn[C_WARN] =  "Insufficient points."
      warning @warn.to_json
    end
    #warning "Insufficient points." if points.count <= 24

    # Calculate average
    avg = (points.inject(0, :+) / points.count).round(2)

    avg, unit_msg = units(avg, config[:check])

    output = "Last #{config[:time]} hours avg: #{unit_msg}"
    ok_res = output
    # Compare with threshold
    if avg <= config[:threshold]
      @crit[C_FAIL] = output
      critical @crit.to_json
    end
    @payload[C_SUCCESS] = ok_res
    ok @payload.to_json 
  rescue => error
    warning error.message
  end

  def units(avg, check)
    case check
    when "cpu.idle"
      # in %
      return avg, "Idle avg #{avg}"
    when "memory.free"
      avg = (avg / 1024 / 1024).round(2)
      # in MB's
      return avg, "Mem free avg #{avg}" 
    else
      return avg, "#{avg}"
    end
  end
end
