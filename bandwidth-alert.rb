#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'json'
require 'sensu-plugin/metric/cli'
require 'socket'

C_SUCCESS = '0'
C_WARN = '1'
C_FAIL = '2'
C_UNKNOWN = '3'

$cloud_fact_file = "/etc/ansible/facts.d/cloud_provider.fact"

class BandwidthChecker < Sensu::Plugin::Metric::CLI::Graphite 

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}.zabbix"

  def initialize
    super
    @output = {}
    @payload = { "type"=> "alert", "output"=>"", "code"=>0, "immediate"=>true }
  end


  def run
    # @payload["type"] = "alert"
    # Bandwidth used in MiB
    if File.readlines($cloud_fact_file).grep(/bandwidth_metric/).any?
      metric = File.readlines($cloud_fact_file).grep(/bandwidth_metric/)
      bw_metric = metric[0].split('=')[1].chomp()
      if bw_metric == ""
        @payload["code"] = 3
        @output[C_UNKNOWN] = "Bandwidth metric not found."
        @payload["output"] = @output.to_json
        ok @payload.to_json
      end

      # get default interface
      default_eth = `route | grep default`
      default_eth = default_eth.split().last()

      # Adding output of vnstat in variable
      vnstat_output = `vnstat -i #{default_eth} --oneline`

      if bw_metric == "incoming"
        used = vnstat_output.split(";")[8].split[0].to_f
        unit = vnstat_output.split(";")[8].split[1]
      elsif bw_metric == "outgoing"
        used = vnstat_output.split(";")[9].split[0].to_f
        unit = vnstat_output.split(";")[9].split[1]
      elsif bw_metric == "total"
        used = vnstat_output.split(";")[10].split[0].to_f
        unit = vnstat_output.split(";")[10].split[1]
      elsif bw_metric == "greater"
        in_used = vnstat_output.split(";")[8].split[0].to_f
        in_unit = vnstat_output.split(";")[8].split[1]
        out_used = vnstat_output.split(";")[9].split[0].to_f
        out_unit = vnstat_output.split(";")[9].split[1]

        in_used = convert_to_mb(in_unit, in_used)
        out_used = convert_to_mb(out_unit, out_used)

        if in_used > out_used
          result = in_used
        else
          result = out_used
        end
      end
    end

    if bw_metric != "greater"
      result = convert_to_mb(unit, used)
    end

    threshold_critical = get_threshold.to_i

    if result >= threshold_critical
      @output = {
        :"#{C_FAIL}" => {
          :threshold_critical => threshold_critical,
          :current => result
        }
      }
      @payload["code"] = 2
      @payload["output"] = @output.to_json
      ok @payload.to_json
     
    end

    threshold_warning = threshold_critical * 80 / 100

    if result >= threshold_warning
      @output = {
        :"#{C_WARN}" => {
          :threshold_warning => threshold_warning,
          :threshold_critical => threshold_critical,
          :current => result
        }
      }
      @payload["code"] = 1 
      @payload["output"] = output.to_json
      ok @payload.to_json

    end
    @payload["code"] = 0
    @output[C_SUCCESS] = "OK"
    @payload["output"] = @output.to_json
    ok @payload.to_json

  rescue => error
    unknown error.message
  end

  def convert_to_mb(unit, used)
      return case unit
    when /KiB/
      used.to_f / 1024
    when /MiB/
      used.to_f
    when /GiB/
      used.to_f * 1024
    when /TiB/
      used.to_f * 1024 * 1024
    else
      nil
    end
  end

  def get_threshold
    # Read facts file and get bandwid threshold
    if File.readlines($cloud_fact_file).grep(/bandwidth_threshold/).any?
      threshold = File.readlines($cloud_fact_file).grep(/bandwidth_threshold/)
      threshold = threshold[0].split('=')[1].chomp()
      if threshold == ""
        @payload["code"] = 3
        @output[C_UNKNOWN] = "Bandwidth threshold not found."
        @payload["output"] = @output.to_json
        ok @payload.to_json
      end
      return threshold
    else
      @payload["code"] = 3
      @output[C_UNKNOWN] = "Threshold not found."
      @payload["output"] = @output.to_json
      ok @payload.to_json
    end
  end

end
