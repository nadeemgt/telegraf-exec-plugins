#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'

C_SUCCESS = '0'
C_WARN = '1'
C_FAIL = '2'

class CheckMySQLStats < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :long => "--scheme SCHEME",
    :default => "cg.#{Socket.gethostname}"

  option :maxwarn,
    description: "Percentage of connections upon which we'll issue a warning",
    short: '-w NUMBER',
    long: '--warnnum NUMBER',
    default: 80

  option :maxcrit,
    description: "Percentage of connections upon which we'll issue an alert",
    short: '-c NUMBER',
    long: '--critnum NUMBER',
    default: 90

  option :user,
    description: 'MySQL User',
    short: '-u USER',
    long: '--user USER',
    default: 'sensu'

  option :password,
    description: 'MySQL Password',
    short: '-p PASS',
    long: '--password PASS',
    default: "#{Socket.gethostname}"
  def initialize
    super
    @warn = {}
    @crit = {}
    @output = {}
    @payload = {"type"=>"alert", "code"=>0, "immediate"=>true, "output"=>""}
  end

  # Execute command line mysql query to retrieve stat
  def mysql_query(query)
    result = `mysql --connect-timeout=5 -u #{config[:user]} -p#{config[:password]} -e "#{query}" -BN 2>/dev/null`.split()[1]
    return result.to_i
  rescue => error
    @payload["code"] = 1
    @output[C_WARN] = error.message
    @payload["output"] = @output.to_json
    ok @payload.to_json
  end

  def run
    max_con = mysql_query("SHOW VARIABLES LIKE 'max_connections'")
    max_used_con = mysql_query("SHOW GLOBAL STATUS LIKE 'max_used_connections'")

    #WARNalid max connections
    if max_con == 0
      @payload["code"] = 1
      @output[C_WARN] = "Max connections = 0, connections possibly full or MySQL down."
      @payload["output"] = @output.to_json
      ok @payload.to_json      
    end

    # Calculate percentage of used connections
    pc = (max_used_con.fdiv(max_con) * 100).round(2)

    # Reset stat if value was critical
    mysql_query("flush status;") if pc >= config[:maxcrit].to_i

    if pc >= config[:maxcrit].to_i
      @payload["code"] = 2
      @output[C_FAIL] = "#{pc}% MySQL connections used. Max used: #{max_used_con} Limit: #{max_con}"
      @payload["output"] = @output.to_json
      ok @payload.to_json
    end
    @payload["code"] = 0
    @output[C_SUCCESS] = "Max connections used is under limit in MySQL: #{max_used_con} out of #{max_con}"
    @payload["output"] = @output.to_json
    ok @payload.to_json
  rescue => error
    @payload["code"] = 1
    @output[C_WARN] = error.message
    @payload["output"] = @output.to_json
    ok @payload.to_json
  end
end
