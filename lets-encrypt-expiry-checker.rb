#!/usr/bin/env ruby

require 'net/https'
require 'sensu-plugin/check/cli'
require 'socket'

$script_path = "/var/cw/scripts/bash/letsencrypt.sh"

def parse_cron(regex, position)
    # Example line to parse in crontab
    # 49 5 1,15 * * /var/cw/scripts/bash/letsencrypt.sh -s mduqgzwzfn -d 30 #letsencrypt_mduqgzwzfn
    output = `cat /etc/crontab | grep #{regex} | awk '{print $(NF-#{position})}' | cut -d '_' -f2`
    users = output.split("\n")
    return users
end

class CheckHttpCert < Sensu::Plugin::Check::CLI

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}"

  option :warning,
         short: "-w",
         long: "--warning DAYS",
         proc: proc(&:to_i),
         description: "Warn EXPIRE days before cert expires"

  option :critical,
         :default => 30,
         short: "-c",
         long: "--critical DAYS",
         proc: proc(&:to_i),
         description: "Critical EXPIRE days before cert expires"

  option :regex,
         :default => "'#letsencrypt_.*'",
         short: "-r",
         long: "--regex STRING",
         description: "Regex string to match for in the crontab"

  option :position,
         :default => 0,
         short: "-p",
         long: "--postion COLUMN",
         proc: proc(&:to_i),
         description: "Position of user name in the crontab from the right (reversed)"

  def run
    users = parse_cron(config[:regex], config[:position])

    crit_fs = []

    users.each do |user|
        output = `#{$script_path} -s #{user} -e`
        if output.empty?
            next
        elsif output.include? "No Let's Encrypt Certificate Available"
            next
        elsif output =~ /^\d+ +\w+$/
            days_until, user_ = output.split(" ")
            days_until = days_until.to_i
        else
            next
        end

        if days_until < config[:critical].to_i
            crit_fs << "Critical user #{user}- #{days_until} days left until expiry."
        end
    end

    ok 'No Lets_Encrypt users found.' unless users.any?
    critical crit_fs unless crit_fs.empty?
    ok 'All ok.'

  rescue => error
    critical error.message
  end
end
