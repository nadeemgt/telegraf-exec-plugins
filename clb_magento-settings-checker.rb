#!/usr/bin/env ruby 

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'json'
require 'socket'

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

# FQDN
APP_TO_CHECK="magento"
FQDN = Socket.gethostname

#$fqdn = `hostname`.chomp
$fails=[]
$unknowns=[]

class AppMagentoSettingChecker < Sensu::Plugin::Metric::CLI::Graphite

option :scheme,
  :description => "Metric naming scheme, text to prepend to .$parent.$child",
  :long => "--scheme SCHEME",
  :default => "#{Socket.gethostname}"

def initialize
  super
  @crit = []
  @output = {}
  @payload = {"type"=>"alert", "immediate"=>true, "code"=>0}
  
end

def run
  # Grep all installed Magneto apps on system
  
  magento_apps = get_apps
  if magento_apps.empty?
      @payload["code"]=0
      @output = {C_SUCCESS => {}}.to_json
  else
      magento_apps_dict = get_app_versions(magento_apps)
      if magento_apps_dict.empty?
        @payload["code"] = 0
        @output = {C_SUCCESS => {}}.to_json
      else
        settings_dict = {}
        magento_apps.each do |app|
    		if magento_apps_dict[app].start_with?('2')
      		   #magneto v2.0 settings
                   settings_dict[app] = app_settings_handler2(app)
    		elsif magento_apps_dict[app].start_with?('U')
      		   #magento version setting unkown
      		   $unknowns.push(app)
                else
                   #magento v1 settings
                   #settings_dict[app] = app_settings_handler(app)
                   next  # Depericated due to EOL by Coudways (Aug-2021)
    		end
        end
        unknown_dict={"version" => "unknown"}
        result = {"0" => {}, "2" => {}, "3" => {}}
        magento_apps.each do |app|
    		if( $fails.include?app)
         		result["2"][app] = settings_dict[app]
    		elsif ( $unknowns.include?app)
         		#result["3"][app] = settings_dict[app]
         		result["3"][app]=unknown_dict
    		else
         		result["0"][app] = settings_dict[app]
    		end
        end
        #print JSON.dump(result)
        if not $unknowns.empty?
	   @payload["code"] = 3
        elsif not $fails.empty?
	   @payload["code"] = 2
        end
        @output = result.to_json
      end
  end
  @payload["output"] = @output
  ok @payload.to_json
  
rescue => error
  @payload["code"] = 1
  @payload["output"]  = error.message
  ok @payload.to_json
end
end

#Here we can add parameters for which we need to check settings
def app_settings_handler2 (app)
  app_settings_dict = {}
  app_settings_parameters=["full_page"]
  app_settings_parameters.each do |parameter|
      app_settings_dict[parameter] = parameter_settings_magento2(parameter,app)  #pass parameters and app fqdn
  end

  return app_settings_dict
end

def parameter_settings_magento2 (parameter,app)
  settings_fetch = {}
  if (parameter === "full_page")
      settings_fetch = get_full_page(app)    
  else
    settings_fetch = {}
    print "No Parameter"
  end
end

def get_full_page(app)
  full_page_settings_dict = {}
  settings_name = ["active"]
  settings_name.each do |setting_name|
    full_page_settings_dict[setting_name]=get_full_page_settings(app,setting_name)
  end

  return full_page_settings_dict
end

def get_full_page_settings(app,setting_name)
  app_root = get_app_root app

  full_page_settings_to_check_dict = {}
  f_name = "#{app_root}/app/etc/env.php"
  if File.file?(f_name) 
    File.open f_name do |file|
      check =  file.find { |line| line =~ /full_page/ }
      if check.nil?
      else
        result=check.split('>')[1].split(',')[0]
        if (result ===" 0")
          $fails.push(app)
          full_page_settings_to_check_dict="false"
        else
          full_page_settings_to_check_dict="true"
        end
      end
    end
  else
    $fails.push(app)
    full_page_settings_to_check_dict="false"
  end
  return full_page_settings_to_check_dict
end


#Here we can add parameters for which we need to check settings
def app_settings_handler (app)
  app_settings_dict = {}
  version = `cat /etc/debian_version`
  if version >= '8.0'
    app_root = get_app_root app
    amasty_file = "#{app_root}/app/etc/modules/Amasty_Base.xml"
    if File.exist?(amasty_file) 
      # Amasty is installed
      app_settings_parameters=["amasty"]
      app_settings_parameters.each do |parameter|
        app_settings_dict[parameter] = parameter_settings(parameter,app)  #pass parameters and app fqdn
      end
    else
      # Turpentine iis installed
      app_settings_parameters=["turpentine"]
      app_settings_parameters.each do |parameter|
        app_settings_dict[parameter] = parameter_settings(parameter,app)  #pass parameters and app fqdn
      end
    end
  else
    app_settings_parameters=["phoenix"]
    app_settings_parameters.each do |parameter|
      app_settings_dict[parameter] = parameter_settings(parameter,app)  #pass parameters and app fqdn
    end
  end



  return app_settings_dict
end

#this will call specific function according to the passed parameter. Should throw error in case of no parameters
#you can add more parameters here later on
def parameter_settings (parameter,app)
  settings_fetch = {}
  if (parameter === "phoenix")
      settings_fetch = get_phoenix(app)
  elsif (parameter === "turpentine")
      settings_fetch = get_turpentine(app)      
  elsif (parameter === "amasty")
      settings_fetch = get_amasty(app)      
  else
    settings_fetch = {}
    print "No Parameter"
  end
end

#this will pass on settings parameters of w3tc, you can append the list down
def get_phoenix(app)
  turp_settings_dict = {}
  settings_name = ["active"]
  settings_name.each do |setting_name|
    turp_settings_dict[setting_name]=get_turp_settings(app,setting_name)
  end

  return turp_settings_dict
end

def get_phoenix_settings(app,setting_name)
  app_root = get_app_root app

  turp_settings_to_check_dict = {}
  f_name = "#{app_root}/app/etc/modules/Phoenix_VarnishCache.xml"
  if File.file?(f_name) 
    File.open f_name do |file|
      check =  file.find { |line| line =~ /<#{setting_name}>/ }
      if check.nil?
      else
        result=check.split('>')[1].split('<')[0]
        turp_settings_to_check_dict=result
        if (result === "false")
          $fails.push(app)
        end
      end
    end
  else
    $fails.push(app)
    turp_settings_to_check_dict="false"
  end
  return turp_settings_to_check_dict
end

def get_turpentine(app)
  turp_settings_dict = {}
  settings_name = ["active"]
  settings_name.each do |setting_name|
    turp_settings_dict[setting_name]=get_turp_settings(app,setting_name)
  end

  return turp_settings_dict
end

def get_turp_settings(app,setting_name)
  app_root = get_app_root app

  turp_settings_to_check_dict = {}
  f_name = "#{app_root}/app/etc/modules/Nexcessnet_Turpentine.xml"
  if File.file?(f_name) 
    File.open f_name do |file|
      check =  file.find { |line| line =~ /<#{setting_name}>/ }
      if check.nil?
      else
        result=check.split('>')[1].split('<')[0]
        turp_settings_to_check_dict=result
        if (result === "false")
          $fails.push(app)
        end
      end
    end
  else
    $fails.push(app)
    turp_settings_to_check_dict="false"
  end
  return turp_settings_to_check_dict
end

def get_amasty(app)
  app_root = get_app_root app

  amasty_file = "#{app_root}/app/etc/modules/Amasty_Base.xml"
  turp_settings_dict = {}
  settings_name = ["active"]
  settings_name.each do |setting_name|
    File.open amasty_file do |file|
      check =  file.find { |line| line =~ /<#{setting_name}>/ }
      if check.nil?
      else
        result=check.split('>')[1].split('<')[0]
        turp_settings_dict[setting_name] = result
        if (result === "false")
          $fails.push(app)
        end
      end
    end
  end
  
  return turp_settings_dict
end

def get_app_root(app)
    v_host_file = "/etc/nginx/sites-available/#{app}"
    _app_root = open(v_host_file) {
      |f| f.grep(/root/)
    }
    app_root = _app_root[0].split()[1].tr(';', '')
  rescue
    return "/home/#{FQDN}/#{app}/public_html"
end

def get_bin_dir(app_root)
      Dir.chdir(app_root)
      if Dir.entries('.').include? "bin"
        Dir.chdir('./bin/')
        return Dir.pwd
      elsif Dir.entries("..").include? "bin"
        Dir.chdir('../bin/')
        return Dir.pwd
      else
        return nil
      end
end
def get_apps()
    # Get apps from nginx vhost to prevent duplicates because of symlinks
    vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}
    apps = []
    vhosts.each do |vhost|
      next unless File.exists?("/home/#{FQDN}/#{vhost}/conf/server.nginx")
      File.open "/home/#{FQDN}/#{vhost}/conf/server.nginx" do |file|
          file.each_line do |line|
              if line =~ /#{APP_TO_CHECK}/
                apps << vhost
                break
              end
          end
      end
    end
    return apps
end

# Returns a dict of magento apps and thier versions
def get_app_versions(magento_apps)
    magento_apps_dict = {}
    # Check each apps version using Mage
    magento_apps.each do |app|
      app_root = get_app_root app
      # Look for magento 2.X installations
      bin_dir = get_bin_dir(app_root)
      if (not bin_dir.nil?) && File.exists?("#{bin_dir}/magento")
        output = `sudo -u #{app} php #{bin_dir}/magento -V --no-ansi`
        if $?.exitstatus != 0
                @warning << "#{app}: #{output}"
                next
        end
        magento_current = output.split(" ")[-1]
        magento_apps_dict[app] = magento_current      
      elsif File.exists?("#{app_root}/app/Mage.php")
        # skip Magento 1 due to EOS EOL
        next
      else
        # If magento version unknown, because mage/cli not found
        magento_apps_dict[app] = "UNKNOWN"
      end
    end
    return magento_apps_dict
end
