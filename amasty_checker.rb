#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
if RUBY_VERSION =~ /1.9/ # assuming you're running Ruby ~1.9
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8
end

require 'sensu-plugin/check/cli' 
require 'json'
require 'socket'

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

# FQDN
$fqdn=`hostname`.chomp

class AppMagentoPluginUsageChecker < Sensu::Plugin::Check::CLI

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"

  option :csv,
    :description => 'Return output in csv',
    :long => "--csv",
    :boolean => true,
    :default => false

  def initialize
    super
    if config[:csv] == true
      @payload = ''
    else
      @payload = {}
    end
  end

  def run
    # Get all magento app names
    magento_apps = get_apps
    ok ({C_SUCCESS => {}}.to_json) if magento_apps.empty?

    # Get versions of apps
    magento_apps_dict = get_app_versions magento_apps

    magento_apps_dict.delete_if {|app, version| version.start_with?("2") }
    magento_apps_dict.delete_if {|app, version| version.start_with?("UNKNOWN") }

    magento_apps_dict.each do |app, version|
      data = get_app_data(app) 

      if config[:csv] == true
        @payload += "#{app},#{data}\n"
      else
        @payload[app] = data
      end
    end
    warning @payload
  rescue => error
    warning error.message
  end

end

def get_app_data(app)
  data = File.open("/home/#{$fqdn}/#{app}/conf/server.nginx").grep(/server_name/)[0].tr(';', '').split()

  # get app-fqdn
  app_fqdn = data[1]

  # get primary domain
  p_domain = 'None'
  if data.count > 2
    p_domain = data[2]
  end

  plugin = get_plugin(app)

  # return csv
  return "#{app_fqdn},#{p_domain},#{plugin}"
end

def get_plugin(app)
  plugin = 'None'
  app_root = get_app_root app
  base_file = "#{app_root}/app/etc/modules"
  version = `cat /etc/debian_version`

  if version >= '8.0'
    amasty_file = "#{base_file}/Amasty_Base.xml"
    turpentine_file = "#{base_file}/Nexcessnet_Turpentine.xml"

    if File.exist?(amasty_file) 
      plugin = 'amasty' if check_plugin_active(amasty_file)
    elsif File.exist?(turpentine_file)
      plugin = 'varnish' if check_plugin_active(turpentine_file)
    end

  else
    phoenix_file = "#{base_file}/Phoenix_VarnishCache.xml"

    if File.exist?(phoenix_file) 
      plugin = 'varnish' if check_plugin_active(phoenix_file)
    end

  end

  return plugin
end

def check_plugin_active(file)
  settings_name = ["active"]
  settings_name.each do |setting_name|
    File.open file do |file|
      check =  file.find { |line| line =~ /<#{setting_name}>/ }
      if check.nil?
        return false
      else
        result = check.split('>')[1].split('<')[0]
        if (result === "false")
          return false
        else 
          return true
        end
      end
    end
  end
  
  return turp_settings_dict
end

def get_apps()
  app_to_check = ["magento"]

  # Get apps from nginx vhost to prevent duplicates because of symlinks
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      file.each_line do |line|
        app_to_check.each do |_app|
          if line =~ /#{_app}/
            apps << vhost
          end
        end
      end 
    end 
  end 
  return apps
end

def get_app_root(app)
  v_host_file = "/etc/nginx/sites-available/#{app}"
  _app_root = open(v_host_file) { 
    |f| f.grep(/root/) 
  }
  app_root = _app_root[0].split()[1].tr(';', '')
rescue
  return "/home/#{$fqdn}/#{app}/public_html"
end

def get_apps()
  apps_to_check = ["magento"]

  # Get apps from nginx vhost to prevent duplicates because of symlinks
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      catch :app_found do
        file.each_line do |line|
          apps_to_check.each do |wp|
            if line =~ /#{wp}/
              apps << vhost
              throw :app_found 
            end
          end
        end 
      end
    end 
  end 
  return apps
end

# Returns a dict of magento apps and thier versions
def get_app_versions(magento_apps)
  magento_apps_dict = {}
  
  # Check each apps version using Mage
  magento_apps.each do |app|
    app_root = get_app_root app

    magento_mage_file = "#{app_root}/app/Mage.php"
    # If magento 1.X
    if File.exists?(magento_mage_file)
      magento_current = `cd #{app_root}/;\
su #{app} -s /bin/bash -c 'php -r "include'"'"'app/Mage.php'"'"'; echo Mage::getVersion();" 2> /dev/null | tail -n1'`.strip()
      magento_apps_dict[app] = magento_current
    else
      # If magento 2.X
      if File.exists?("#{app_root}/bin/magento")
        magento_current = `cd #{app_root}/bin;\
su #{app} -s /bin/bash -c "php magento -V --no-ansi 2> /dev/null | cut -d ' ' -f4"`.strip()
        magento_apps_dict[app] = magento_current
      # If magento version unknown, because mage/cli not found
      else
        magento_apps_dict[app] = "UNKNOWN" 
      end
    end
  end
  return magento_apps_dict
end
