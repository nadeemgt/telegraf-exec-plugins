#!/usr/bin/env ruby
# -------

require 'open3'
require 'json'
require 'sensu-plugin/metric/cli'
require 'socket'

C_SUCCESS = '0'
C_FAIL = '2'

class WebStack < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :long => "--scheme SCHEME",
    :default => "cg.#{Socket.gethostname}"

  option :check_type,
    :description => "The varnishd instance to get data from",
    :short => "-c CHECK_TYPE",
    :long => "--check CHECK_TYPE",
    :default => "graphite"
  def initialize
    super
    # @crit = {}
    @output = {}
    @payload = { "type"=>"alert", "immidiate"=>false, "code"=>0, "output"=>"" }
  end

  def run
    def is_port(port, process)
      return false unless File.exists?("/var/run/#{process}.pid")
      stdout, stderr, exit_status = Open3.capture3(
        ENV,
        "nc -vz -w5 -q0 127.0.0.1 #{port}"
      )
      exitstatus = (exit_status.success? ? 'succeeded' : 'failed')    
      return false unless exitstatus['succeeded']
      return true
    end

    def fpm_check
      count = `ps -ef | grep -v grep  | grep fpm.*master -c`
      if count.to_i > 0
        return true
      else
        return false
      end
    end

    def php_mod_check
      count = `grep -e "fpm=enable" /etc/ansible/facts.d/packages.fact -c`
      if count.to_i > 0
        return false
      else
        return true
      end
    end

    def ngx_check
      Dir.glob('/etc/nginx/sites-enabled/*')  do |e|
        return is_port(80, "nginx")
      end
      return true
    end

    def varnish_check 
      varnish_file = "/etc/monit/services.cloudways/varnish"
      if File.exist?(varnish_file)
        if File.readlines(varnish_file).grep(/monitor=yes/).any?
          return is_port(8080, "varnishd")
        end
      end
      return true
    end

    def debian_release
      `lsb_release -c`
    end

    def apache_check
      release = debian_release
      if release["wheezy"]
        return is_port(8081, "apache2")
      elsif release["stretch"] or release["jessie"]
        return is_port(8081, "apache2/apache2")
      else
        return false
      end
      return true
    end


    count = 0
    check = {}

    if not php_mod_check
      check['Php-fpm'] = fpm_check.eql?(true) ? 1 : 0
    end
    check['apache'] = apache_check.eql?(true) ? 1 : 0
    check['nginx'] = ngx_check.eql?(true) ? 1 : 0
    check['mysql'] = is_port(3306, "mysqld/mysqld").eql?(true) ? 1 : 0
    check['varnish'] = varnish_check.eql?(true) ? 1 : 0
    check['memcached'] = is_port(11211, "memcached").eql?(true) ? 1 : 0

    check.map { |k, i| i.to_i }.each {|i| count += i}
    webstackhealth = check.length.eql? count
    check['webstack'] = webstackhealth.eql?(true) ? 1 : 0
    case config[:check_type]
    when 'graphite'
      output "#{config[:scheme]}.webstack.health", webstackhealth.eql?(true) ? 1 : 0
      ok
    when 'handler'
      result = {}
      if webstackhealth.eql?(true)
        @output[C_SUCCESS] = check.keys
        @payload["output"] = @output.to_json
        ok @payload.to_json
      else
        check.each do |k, v|
          if v == 0
            if result.key?(C_FAIL)
              result[C_FAIL] << "#{k}" 
            else
              result[C_FAIL] = ["#{k}"] 
            end
          else
            if result.key?(C_SUCCESS)
              result[C_SUCCESS] << "#{k}"
            else
              result[C_SUCCESS] = ["#{k}"] 
            end
          end
        end
        @payload["code"]=2
        @payload["output"]=result.to_json
        ok @payload.to_json
      end
    end 

  end
end
