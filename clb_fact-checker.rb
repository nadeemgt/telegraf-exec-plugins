#!/usr/bin/env ruby

require 'json'
require 'sensu-plugin/check/cli'
require 'socket'


# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

class CheckFact < Sensu::Plugin::Check::CLI

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}"

  option :regex,
         :default => "nginx_http2",
         short: "-r",
         long: "--regex STRING",
         description: "Regex string to match for in the facts file, default: nginx_http2"

  option :facts_file,
         :default => "/etc/ansible/facts.d/server_settings.fact",
         proc: proc { |a| a = "/etc/ansible/facts.d/#{a}.fact" },
         short: "-f",
         long: "--file NAME",
         description: "Name of fact file to check, default: server_settings (/etc/ansible/facts.d/server_settings.fact)"

  def run  
    payload = {}

    if File.file?(config[:facts_file])
      if File.readlines(config[:facts_file]).grep(/#{config[:regex]}=enable/).size > 0
        payload[C_SUCCESS] = "#{config[:regex]} enabled."

      elsif File.readlines(config[:facts_file]).grep(/#{config[:regex]}=disable/).size > 0
        payload[C_FAIL] = "#{config[:regex]} disabled."

      elsif File.readlines(config[:facts_file]).grep(/#{config[:regex]}/).size > 0
        payload[C_FAIL] = "#{config[:regex]} present but invalid status."

      else
        payload[C_FAIL] = "#{config[:regex]} disabled."
      end

    else
        payload[C_FAIL] = "#{File.basename(config[:facts_file])} fact file not found"
    end

    critical payload.to_json unless payload.key?(C_SUCCESS)
    ok payload.to_json
  rescue => error
    critical error.message
  end
end
