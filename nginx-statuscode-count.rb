#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'
require "date"
require 'zlib'

$log_name = "nginx-app.status.log"
$codes = {}

class CheckNginxAppStats < Sensu::Plugin::Metric::CLI::Graphite

  #option :scheme,
  #       :description => "Metric naming scheme, text to prepend to metric",
  #       :short => "-s SCHEME",
  #       :long => "--scheme SCHEME",
  #       :default => "#{Socket.gethostname}.nginx.status_code"

  option :days,
         :default => 3,
         :short => "-d",
         :long => "--days INT",
         :proc => proc(&:to_i),
         :description => "The number of days to check."

  option :status_code,
         :default => "all",
         :short => "-c",
         :long => "--code STRING",
         :description => "Nginx HTTP status code"

  def initialize
    super
    # set regex for http status code
    @status_code_rgx = case config[:status_code]
        when /5x/
                /5\d{2}/
        when /50x/
                /50\d{1}/
        when /4x/
                /4\d{2}/
        when /40x/
                /40\d{1}/
        when /3x/
                /3\d{2}/
        when /30x/
                /30\d{1}/
        when /2x/
                /2\d{2}/
        when /20x/
                /20\d{1}/
        else
                /\d{3}/
        end
  end

  # Remove all files whose mtime falls outside last d days.
  def filter_files(files)
    files.delete_if { |l| date_not_within_range?(File.mtime(l).to_date) }
  end

  # Return hash of occurrences of each item in array
  # Just like "uniq -c" in bash
  def hash_uniq_count(hash)
    counts = Hash.new(0)
    hash.each { |name| counts[name] += 1 }

    return counts
  end

  # Check if date withing range of days to check
  # i.e. within last x days
  def date_not_within_range?(check_date)
    days = config[:days]
    return Date.today - days >= check_date
  end

  # Process log line
  # Extract code, date, ip, etc.
  # Only append to codes if date within range
  def handle_line(l)
    # if status code in line...
    if l =~ /status_code:\d+/
      # Sample line
      # status_code:503 110.93.203.98 [02/Dec/2016:10:31:32 +0000] \
      # GET / HTTP/1.1
      status, ip = l.split[0..1]
      date = l.match(/status_code:\d+ \b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b \[(.*)\] \w+ .*/).captures[0]
      entry_time = DateTime.strptime(date, '%d/%b/%Y:%H:%M:%S %z')

      last_day = Date.today - 1 

      # check if date within range
      if entry_time.to_date == last_day
        code = status.split(':')[1]

        # check if code matches status code regex
        if code =~ /#{@status_code_rgx}/
          $codes[code] = 0 unless $codes.has_key?(code)
          $codes[code] += 1
        end
      end
    end 
  rescue => error
    return
  end

  def run
    hostname = "#{Socket.gethostname}.nginx.status_code"
    # Get all files names matching our log
    logs = Dir.glob("/home/#{Socket.gethostname}/**/logs/#{$log_name}*")

    # Filter for those which were modified within the last x days
    logs = filter_files(logs)

    # Iterate all log files
    logs.each do |log|
      # opening .gz file
      if log.include?('.gz')
        infile = open(log)
        f = Zlib::GzipReader.new(infile)
      # opening regular file...
      else
        f = File.open(log)
      end

      f.each_line do |l|
        handle_line(l)
      end
    end

    timestamp = Time.now.to_i
    for code, val in $codes
      # ouput data for graphite
      output "nginx.status_code,type=metric #{code}=#{val} #{timestamp}"
    end
    ok
  end
end
