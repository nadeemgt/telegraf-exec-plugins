#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'date'
require 'sensu-plugin/metric/cli'
require 'socket'

class Snapshot < Sensu::Plugin::Metric::CLI::Graphite
  option :scheme,
         :description => "Metric naming scheme, text to prepend to .$parent.$child",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}"

  def run
    # fact file path
    filename = '/etc/ansible/facts.d/cloud_backup.fact'

    # Status
    status = `cat #{filename} | grep "^status" | cut -d'=' -f2`.strip()
    ok "Snapshots disable" if status == "disable"

    # Frequency
    frequency = `cat #{filename} | grep "frequency" | cut -d'=' -f2`

    # Checking Time
    time24 = `date -d "#{frequency} hours ago" '+%Y-%m-%d %T'`
    cutoff_epoch = `date +%s -d "#{time24}"`

    # If last_backup exists
    if File.readlines(filename).grep(/last_backup/).any?
      # last_backup = 01/18/2010 00:00 UTC
      last_backup = File.readlines(filename).grep(/last_backup/)
      last_backup = last_backup[0].split('=')[1].strip()
      last_backup_epoch = Date.strptime("#{last_backup}",
                                        '%d/%m/%Y %H:%M %Z').to_time.to_i

      if last_backup_epoch < cutoff_epoch.to_i
        critical "Snapshot Backup Expired."
      else
        ok "All OK"
      end
    else
      unknown "Not Backed up yet."
    end
  rescue => error
    critical error.message
  end
end
