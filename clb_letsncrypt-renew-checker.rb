#!/usr/bin/env ruby

require 'json'
require 'net/https'
require 'sensu-plugin/metric/cli'
require 'socket'

# Codes
C_SUCCESS = '0'
C_FAIL = '2'
C_UNKNOWN = '3'

$script_path = "/var/cw/scripts/bash/letsencrypt.sh"
$facts_file = "/etc/ansible/facts.d/letsencrypt.fact"

def parse_cron(regex, position)
    # Example line to parse in crontab
    # 49 5 1,15 * * /var/cw/scripts/bash/letsencrypt.sh -s mduqgzwzfn -d 30 #letsencrypt_mduqgzwzfn

    users = []
    File.open('/etc/crontab').each do |line|
        if line =~ /#{regex}/ 
            users << line.split(' ')[position].split('_')[1]
        end
    end
    return users
end

class CheckHttpCert < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}"

  option :warning,
         short: "-w",
         long: "--warning DAYS",
         proc: proc(&:to_i),
         description: "Warn EXPIRE days before cert expires"

  option :critical,
         :default => 30,
         short: "-c",
         long: "--critical DAYS",
         proc: proc(&:to_i),
         description: "Critical EXPIRE days before cert expires"

  option :regex,
         :default => "#letsencrypt_.*",
         short: "-r",
         long: "--regex STRING",
         description: "Regex string to match for in the crontab"

  option :position,
         :default => -1,
         short: "-p",
         long: "--postion COLUMN",
         proc: proc(&:to_i),
         description: "Position of user name in the crontab from the right (reversed)"

  def initialize
    super
    @output={}
    @payload = {"type"=>"alert", "code"=>0, "immediate"=> true, "output"=>""}
  end

  def run
    users = parse_cron(config[:regex], config[:position])
    if not File.exists?($facts_file)
    	@payload["code"] = 2
    	@output[C_FAIL]= "Facts file not found"
    	@payload["output"]= @output.to_json	
    	ok @payload.to_json
    end
    #critical ({C_UNKNOWN => "Facts file not found"}.to_json) unless File.exists?($facts_file)

    users.each do |user|
        if File.readlines($facts_file).grep(/#{user}/).count > 0
            status_line = File.readlines($facts_file).grep(/#{user}/)
            code = status_line[0].split('=')[1].chomp()

            if ['enable', 'disable'].include?(code)
                insert_payload(C_SUCCESS, user, "ok")
            else
                insert_payload(C_FAIL, user, code)
            end
        else
            insert_payload(C_UNKNOWN, user)
        end
    end

    #ok ({C_SUCCESS => {}}.to_json) if @payload.empty?
    #ok @payload.to_json if ! @payload.has_key?(C_UNKNOWN) and ! @payload.has_key?(C_FAIL) 
    #critical @payload.to_json
    @payload["output"] = @output.to_json
    ok @payload.to_json
  rescue => error
    ok error.message
  end

  def insert_payload(status, app, code=0)
    @output[status] = {} unless @payload.key?(status)
    @output[status][app] = code
    @payload["code"] = status.to_i unless @payload["code"] >= status.to_i
  end
end
