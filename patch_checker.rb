#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'
require 'time'

#C_SUCCESS = '0'
#C_WARN = '1'
#C_FAIL = '2'
#C_UNKNOWN = '3'

FILEPATH='/etc/ansible/facts.d/patching.fact'
FREQUENCY=30

class BackUP < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
    :description => "Metric naming scheme, text to prepend to .$parent.$child",
    :long => "--scheme SCHEME",
    :default => "#{Socket.gethostname}"


  def initialize
    super
    @crit_fs = []
    @crit = {}
    @unknown = {}
    @line_count = 0
    @output = {}
    @payload = { "type"=>"engg_analytic", "code"=>0, "output"=>"" }
  end


  def run
    if not File.exists?(FILEPATH)
      @output["2"] = "Facts file not found."
      @payload["output"]= @output.to_json
      @payload["code"] = 2
      ok @payload.to_json
    else
      facts = File.readlines(FILEPATH)
      patches = facts.grep(/^last_patched/)
      if patches.length() < 1
        @output["3"] = "No entry in fact file."
        @payload["code"] = 3
        @payload["output"]= @output.to_json
        ok @payload.to_json
      else
        ts_expected=Time.now.to_i - (FREQUENCY * 86400)
        patches.each do |patch|
          patch_date = patch.split("=",0).last.strip
          if patch_date.nil? || patch_date == ""
            @output["2"] = "No last patch date available in fact file"
            @payload["output"]= @output.to_json
            @payload["code"] = 2
            ok @payload.to_json
          else
            timestamp=Time.strptime(patch_date,"%d/%m/%Y").to_i
            if timestamp < ts_expected
              @output["2"] = "No patching in last #{FREQUENCY} days"
              @payload["code"] = 2
              @payload["output"]= @output.to_json
              ok @payload.to_json
            end
          end
        end
        @output["0"] = "Patching is done under last #{FREQUENCY} days"
        @payload["output"]= @output.to_json
        @payload["code"] = 0
        ok @payload.to_json
      end
    end
  end
end



    ## FQDN
    #fqdn=`hostname`.chomp
#
#
    ## Current date
    #now = `date +"%m/%d/%Y"`
    #epoch = `date +%s -d "#{now}"`
#
    ## fact file path
    #filename = '/etc/ansible/facts.d/patching.fact'
#
#
    ## Frequency
    #frequency = "30"
#
#
    ## Checking Time
    #time24 = `date -d "#{frequency} days ago" '+%Y-%m-%d'`
    #time_epoch = `date +%s -d "#{time24}"`
#
#
    #user_date_dict = {}
#
    #filename_read = File.read(filename)
#
#
    ## Checking Cloudways facts
    #if File.exists?(filename)
#
    #  File.readlines(filename).each do |line|
    #    next if line !~ /^last_patched/
    #    begin
    #      pdate = line.split('=', 0).last.strip
    #      if pdate.nil? || pdate == ''
    #        @line_count += 1
    #        @crit_fs << "No last patch date available in fact file"
    #        @crit[C_FAIL] = @crit_fs
    #      else
    #        user_date_dict["date"] = pdate
    #      end
    #    end
    #  end
    #end
#
    #user_date_dict.each do |k, datefile|
    #  @line_count += 1
#
    #  con = `echo #{datefile} | awk '{split($0,a,"/");print a[2]"/"a[1]"/"a[3]}'`
    #  d = `date +%s -d "#{con}"`.strip
#
    #  if time_epoch > d
    #    @crit_fs << "No patching in last #{frequency} days"
    #    @crit[C_FAIL] = @crit_fs
    #  end
    #end
    #@unknown[C_UNKNOWN] = 'No entry in fact file'
    #unknown @unknown.to_json unless @line_count > 0 
    #critical @crit.to_json unless @crit_fs.empty?
    #@payload[C_SUCCESS] = "Patching is done under last #{frequency} days"
    #ok @payload.to_json
#  end
#end

