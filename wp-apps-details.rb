#!/usr/bin/env ruby
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'time'
require 'socket'
require 'uri'
# Codes

C_SUCCESS = '0'
NOT_FOUND = "Version not found"
TIMEOUT = 60
DETAIL_FILE="/etc/telegraf/plugins/data/wp_apps_details.json"
MAP_FILE="/etc/telegraf/plugins/data/wp_detail_map.json"
MAP_URL="https://cloudways-static-content.s3.amazonaws.com/applications/wordpress/plugins_themes/details_map.json"

# FQDN
$fqdn = `hostname`.chomp

class WordpressAppsDetails < Sensu::Plugin::Metric::CLI::Graphite
  
  option :app_count,
    :description => "Number of application to check",
    :short => "-a integer",
    :long => "--applications integer",
    :default => "5"
  option :update_map,
    :description => "whether to update plugin map or not",
    :short => "-u  true|false",
    :long => "--update true|false",
    :default => false

  option :timeout,
    :description => "wpcli timeout seconds",
    :short => "-t integer",
    :long => "--timeout integer",
    :default => 60

    def initialize
      super
      @output={}
      @payload = { "type"=>"business_analytics", "queue"=>"cw-wp-apps-details-cg", "code"=>0, "output"=>"" }
    end

  def run
    
    wp_apps = get_apps
    if wp_apps.empty?
      @payload["output"]=@output.to_json
      ok @payload.to_json
    end
    apps_detail, problem = get_apps_detail(wp_apps)
    if problem
      @payload["code"]=2
    end
    @output = apps_detail
    @payload["output"]= @output.to_json
    ok @payload.to_json
  end

end # class WordpressAppsDetails ends

def get_apps_detail(apps)
  apps_log_hash=get_history apps
  map=load_map config[:update_map]
  problem = false
  fqdn ="#{$fqdn}"
  details_hash={}
  details_hash[fqdn] = {}
  timeout = config[:timeout].to_i
  if config[:app_count].to_i > apps.length()
     count = apps.length()
  else
     count = config[:app_count].to_i
  end
  for cc in 1..count
    min_val = apps_log_hash.values.min
    apps_with_min = apps_log_hash.select { |_, v| v == min_val }
    app = apps_with_min.keys[0]
    begin
      app_root = get_app_root app
      this_app = {}

      app_version = `cd #{app_root}/; \
      timeout #{timeout} sudo -u #{app} wp core version 2> /dev/null`.strip()
      raise "Something went wrong. (#{$?.exitstatus})" if $?.exitstatus != 0
      this_app["core"] = app_version.to_s

      plugins =  `cd #{app_root}/; \
      timeout #{timeout} sudo -u #{app} wp plugin list --fields=name,version,update_version,status --json 2> /dev/null`
      raise "Something went wrong. (#{$?.exitstatus})" if $?.exitstatus != 0

      if plugins != ""
        this_app["plugins"] = transform(plugins,map["plugins"])
      end

      themes = `cd #{app_root}/; \
      timeout #{timeout} sudo -u #{app} wp theme list  --fields=name,version,update_version,status --json 2> /dev/null`
      raise "Something went wrong. (#{$?.exitstatus})" if $?.exitstatus != 0

      if themes != ""
        this_app["themes"] = transform(themes,map["themes"])
      end
      if ! this_app.empty?
        details_hash[fqdn][app] = this_app
      end
      apps_log_hash[app]=Time.now.to_i
    rescue => error
      problem = true
      details_hash[fqdn][app] = error.message
      apps_log_hash[app]=Time.now.to_i
    end
  end
  write_to_file apps_log_hash
  #app_det = JSON.generate(details_hash)
  return details_hash, problem
end

def transform(json_input,map)
  begin
    in_hash = JSON.parse(json_input)
    out_hash={"active"=>{}, "dropin"=>{}, "inactive"=>{}, "must-use"=>{}, "parent"=>{}, "active-network"=>{}, "other"=>{} }
    in_hash.each do |item|
      t = item["name"]
      next if t == ""
      t=map[t] if map.key?(t)
      s = item["status"]
      s = "other" unless out_hash.keys.include?(item["status"])
      out_hash[s][t] = []
      v = item["version"]
      out_hash[s][t][0] = v
      uv = item["update_version"]
      out_hash[s][t][1] = uv
   end
   out_hash.keys.each do |item|
     out_hash.delete(item) if out_hash[item] == {}
   end # Clean up to reduce payload
   return out_hash
  rescue
    return {}
  end    
end

def load_map(update)
  begin
    if ! File.file?("#{MAP_FILE}") # if file does not exists
       update = true
    elsif ( (Time.now - File.mtime("#{MAP_FILE}")) > ( 15 * 86400 ) )
       update = true
    end
    `timeout 10 wget --no-use-server-timestamps #{MAP_URL} -qO #{MAP_FILE}` if update
    raise "Something went wrong. (#{$?.exitstatus})" if $?.exitstatus != 0
    f = open(MAP_FILE)
    map = JSON.parse(f.read)
    return map   
  rescue 
    return {"plugins"=>{}, "themes"=>{} }
  end
end


def get_app_root(app)
  v_host_file = "/etc/nginx/sites-available/#{app}"
  _app_root = open(v_host_file) { 
    |f| f.grep(/root/) 
  }
  app_root = _app_root[0].split()[1].tr(';', '')
rescue
  return "/home/#{$fqdn}/#{app}/public_html"
end

def get_apps()
  wp_to_check = ["wordpress", "woocommerce", "wordpressmu"]
  vhosts = Dir.entries("/etc/nginx/sites-enabled/").select {|f| !File.directory? f}

  wp_apps = []
  vhosts.each do |vhost|
    File.open "/home/#{$fqdn}/#{vhost}/conf/server.nginx" do |file|
      catch :app_found do
        file.each_line do |line|
          wp_to_check.each do |wp|
            if line =~ /#{wp}/
              wp_apps << vhost
              throw :app_found 
            end
          end
        end 
      end
    end 
  end 
  return wp_apps
end

def get_history(apps)
  begin
      f = File.open(DETAIL_FILE,"r")
      apps_log_hash=JSON.parse(f.read)
      apps_log_hash = add_remove(apps_log_hash, apps)
  rescue
      apps_log_hash = {}
      apps.each do |app|
        apps_log_hash[app] = 0
      end
  ensure
    f.close unless f.nil?
  end
  return apps_log_hash
end

def add_remove(apps_log_hash, apps)
    apps.each do |app|
      if (! apps_log_hash.keys.include? app) 
        apps_log_hash[app] = 0
      end
    end
    (apps_log_hash.keys).each do |key|
      if (! apps.include? key)
        apps_log_hash.delete(key)
      end
    end
    return apps_log_hash
end


def write_to_file(apps_log_hash)
  begin
    f = File.open(DETAIL_FILE, "w")
    f.write(JSON.generate(apps_log_hash))
  rescue IOError => e
    puts "IO error while writing app list"
  ensure
    f.close unless f.nil?
  end
end

